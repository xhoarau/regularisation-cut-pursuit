import math
import random
import numpy as np
from functools import partial
import mock
import copy
#format libraries
import h5py
#visualization libraries
import matplotlib.pyplot as plt
import matplotlib.colors as col
from mpl_toolkits import mplot3d
#machine learning tools
from sklearn.neighbors import NearestNeighbors
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix
#deep learning tools
import torch
import torchnet as tnt
import torch.nn.functional as nnf
import torch.nn as nn
import torch.optim as optim
from torchvision import ops
from torch_scatter import scatter_mean
from torch_scatter import scatter_sum
from tqdm import tqdm
import sys,os

from torch.autograd import Function
from torch.autograd.function import once_differentiable
from torch.nn.modules.utils import _pair

# sys.path.append(os.path.join(os.getcwd(),"grid-graph/python/bin"))
# sys.path.append(os.path.join(os.getcwd(),"parallel-cut-pursuit/python/wrappers"))

# from grid_graph import grid_to_graph
# from grid_graph import edge_list_to_forward_star
# from cp_prox_tv import cp_prox_tv

import time

import multiprocessing as mp

import threading as th

from PIL import Image
from PIL import ImageOps
import GPUtil
import modules.save_result as sr
import modules.storage_class as sc
import modules.insertion_donne as insert
import modules.train_fonctions as tf
import modules.Confusion_Matrix as CM
import modules.network_definition as net_def
import modules.plot_evol as pl_ev
import modules.plot_exemple_result as pl_re
import os

import numpy as np
import torch

name = "reseau_lambda_dif_fini_randWL_minComp_compo_logit"

path = "Models/"
if os.path.exists(path):
  # resultat = sr.load(path + "resultat_" + name + ".pt")
  args = sr.load(path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    # resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    # resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")




args_2 = sc.argument(args.n_epoch,args.batch_size,args.n_class,args.class_names,args.n_channels,args.conv_width,args.cuda,args.lr,args.coef_drop,args.n_epoch_validation,[256,256], \
                  args.do_reg,args.blocage_epoch,args.blocage_reg,args.coef_reg,args.nb_feature,args.reg_sousgrad,args.red_grad_lambda, \
                  args.do_augment_front,True,args.prop_degrad,args.prop_img_train_set,args.urban)

# caractéristique réseau
n_epoch = 100 # nombre maxixmum d'epoch pour l'apprentissage
batch_size = 16
n_class = 2
class_names = ["Urban", "not Urban"]
n_channels = 4
conv_width = [32,32,16]
coef_weight = 1 # coefficient reglant l'equilibrage des classes [0,1] ( Si 0 => 1/n_classe; Si 1 => equilibrage en fonction du nombre de pixels)
cuda = 1
lr = 5e-3
coef_drop = 0.5
n_epoch_validation = 8

# caractéristique regularisation
do_reg = True
taille_graph = [256,256] #taille spacial des données
blocage_epoch = 5 # nombre d'époch sans régularisation
# 10  trop grand le réseau est trop précis
blocage_reg = 0 # nombre d'epoch sans contrôle des lambdas (aprés blocage epoch)
coef_reg = 0 # poid du controle des lambda dans la loss
# petit nombre de composante => 10% des arrète coupé sois un coût sur 115000 arrète
# 94% des pixels deja bien prédis => gain possible estimé 1%
# coef reg < 1% / 115000 = 9e-8 sois environ 1e-7
nb_feature = 1/2 # proportion de feature donnée au lambda ([0,1])
reg_sousgrad = False #choix controle Ridge ou Subgrad (si coef_reg = 0 mettre False pour economie de memoire)
red_grad_lambda = 10 #nombre d'epoch de reduction des gradiants de lambda (aprés blocage epoch)
do_augment_front = False # augmenté la loss au frontière de class (mettre Faux si coef_front = 0 economie de memoire)
coef_front = 1 # coefficient reglant l'importance des pixels au frontière [0,1]
logit = True

# caractéristique données
prop_degrad = 0 # proportion de dégradation de l'etiquetage ([0,1])
prop_img_train_set = 1 # nombre d'imga dans l'ensemble d'entrainement (160 * [0,1])
urban = True # choix entre des donnée urban/not urban or road/not road

test_set, train_set, validation_set = insert.creation_set(args)# génération des ensembles de donnée

# recalcul des poids des classes dans la loss
loader = torch.utils.data.DataLoader(train_set, batch_size=1, shuffle=False, drop_last=False)
compt_pixel_class = torch.zeros([n_class])
compt_pixel_front = torch.zeros(1)
if cuda:
  compt_pixel_front = compt_pixel_front.cuda()
for index, (tiles, gt) in enumerate(loader):
  unique,count = torch.unique(gt, sorted=True, return_counts=True)
  if torch.any(unique == 99):
    unique = unique[:-1]
  compt_pixel_class = compt_pixel_class + count[unique]
  
  px_front = torch.zeros(gt.shape).bool()
  if cuda:
    gt = gt.cuda()
    px_front = px_front.cuda()
  for hor in range(-1,1+1):
    for ver in range(-1+abs(hor),1-abs(hor)+1):
      if ver !=0 or hor !=0:
        gt_temp = gt+1
        if hor>0:
          gt_temp = tf.net_def.droite(hor,gt_temp)
        else:
          gt_temp = tf.net_def.gauche(-hor,gt_temp)
        if ver>0:
          gt_temp = tf.net_def.haut(ver,gt_temp)
        else:
          gt_temp = tf.net_def.bas(-ver,gt_temp)
        px_front = px_front + (gt == (1-(gt_temp-1)))
  compt_pixel_front = compt_pixel_front + torch.count_nonzero(px_front)
total_px = torch.sum(compt_pixel_class)
weight_class = 1/n_class + ((total_px - compt_pixel_class)/(total_px*(n_class-1))-1/n_class)*coef_weight
facteur_front = ((total_px-compt_pixel_front)/compt_pixel_front)*coef_front
if cuda:
  args_2.weight_class = weight_class.cuda()
else:
  args_2.weight_class = weight_class

path = "Models/"
if os.path.exists(path):
  # sr.save(resultat, path + "resultat_" + name + ".pt")
  sr.save(args_2, path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    # sr.save(resultat, path + "resultat_" + name + ".pt")
    sr.save(args_2, path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    # sr.save(resultat, path + "resultat_" + name + ".pt")
    sr.save(args_2, path + "args_" + name + ".pt")