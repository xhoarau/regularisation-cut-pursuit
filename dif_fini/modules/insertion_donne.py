import numpy as np
import torch
import torchnet as tnt
import h5py
from functools import partial
import os

#############################################################
# data loader
#############################################################

def tile_loader(tile_index, set_obs, set_gt, args):
  """
  load a tile and returns the observation and associated ground truth
  INPUT:
  tile_index = int, index of the tile
  train = bool, train = True iff in the train set
  OUTPUT
  obs, [4x256 x 256] float Tensor containing the observation
  gt, [256 x 256] long Tensor, containing the pixels semantic labels
  """
  obs = set_obs[tile_index,:,:,:].transpose(2,0,1) #put channels first
  gt = set_gt[tile_index,:,:]

  if args.n_class == 2:
    if not hasattr(args,"square") or not args.square:
      if args.urban:
        gt = 1*(gt==0)+1*(gt==3)+1*(gt==5)+99*(gt==99) # transformation en modèle binaire
      else:
        gt = 1*(gt==3)+99*(gt==99) # transformation en modèle binaire
  else:
    assert args.n_class == 6
  
  #create torch tensors
  obs = torch.from_numpy(obs)
  gt = torch.from_numpy(gt)
   
  return obs, gt.long() #ground truth must have long int type

#############################################################
#Insertion des données
#############################################################
def creation_set(args, rand_choice = True):
  """
  load teh data and create the training, testing and validation set
  INPUT:
  args(class argument) : hold the parameters of the model
    proportion of degradation
    nb img for training
    number of class
    typr of regroupement (urban/non urban or road/not road)
  rand_choice(booléen) : if True the training and validation set are randomly created
    if False : the training and the validation always take the same images
  OUTPUT:
  test_set, obs and gt *100
  train_set, obs and gt *nb img for training (max = 160)
  validation_set, obs ad gt *40
  """

  if hasattr(args,'set_name'):
    filename = args.set_name + ".hdf5"
  else:
    filename = r"land_cover.hdf5"
  if os.path.isfile(filename):
    data_file = h5py.File(filename,'r')
  else:
    if hasattr(args,'set_name'):
      filename = os.path.join(os.getcwd(),os.pardir,args.set_name + ".hdf5")
    else:
      filename = os.path.join(os.getcwd(),os.pardir,"land_cover.hdf5")
    
    if os.path.isfile(filename):
      data_file = h5py.File(filename,'r')
    else:
      assert False and "pas de fichier de donnée"
  
  train_obs = data_file['train_observation'][:]
  train_gt = data_file['train_gt'][:]
  test_obs = data_file['test_observation'][:]
  test_gt = data_file['test_gt'][:]
  n_train = train_obs.shape[0]
  n_test = test_obs.shape[0]

  # dégradation de l'etiquetage
  indice_degrade=np.random.choice(train_gt.size,int(train_gt.size*args.prop_degrad),replace=False)
  train_gt.reshape([train_gt.size])[indice_degrade] = 99
  if args.n_channels == 1:
    train_obs = train_obs.reshape([train_obs.shape[0],train_obs.shape[1],train_obs.shape[2],1])
    test_obs = test_obs.reshape([test_obs.shape[0],test_obs.shape[1],test_obs.shape[2],1])
  print("%d tiles for training, %d tiles for testing" % (n_train, n_test))
  print("Each tile is of size: %d x %d x %d" % (train_obs.shape[1:4]))

  print("data ok")

  if rand_choice:
    #create train and test dataset with ListDataset
    valisation_List = np.random.choice(n_train,int(n_train/5), replace=False)# choisi aléatoirement 20% du set d'entrainement
    #réduction des données d'aprentissage
    List_temp = np.setdiff1d(list(range(n_train)),valisation_List)# prend les 80% restant
    train_List = np.random.choice(List_temp,int((4*n_train/5)*args.prop_img_train_set), replace=False) # choisi aléatoirement une portion des 80% restant
  else:
    #create train and test dataset with ListDataset
    valisation_List = np.arange(int(4*n_train/5),n_train,1)# choisi aléatoirement 20% du set d'entrainement
    #réduction des données d'aprentissage
    train_List = np.arange(0,int(4*n_train/5)*args.prop_img_train_set,1) # choisi aléatoirement une portion des 80% restant

  test_set  = tnt.dataset.ListDataset(list(range(n_test)),partial(tile_loader, set_obs = test_obs, set_gt = test_gt, args=args))
  train_set = tnt.dataset.ListDataset(train_List,partial(tile_loader, set_obs = train_obs, set_gt = train_gt, args=args))
  validation_set = tnt.dataset.ListDataset(valisation_List,partial(tile_loader, set_obs = train_obs, set_gt = train_gt, args=args))
  print("set ok")

  return test_set, train_set, validation_set




