import numpy as np
import torch
import torch.nn as nn
from torch.autograd.function import once_differentiable
from torch_scatter import scatter_mean
from torch.autograd import Function
import copy
import sys,os

sys.path.append(os.path.join(os.getcwd(),"parallel-cut-pursuit/python/wrappers"))
sys.path.append(os.path.join(os.getcwd(),os.pardir,"parallel-cut-pursuit/python/wrappers"))
from cp_prox_tv import cp_prox_tv

import multiprocessing as mp
import threading as th

repartition_lambda = []
std_logit = []

def get_repart():
  return repartition_lambda,std_logit

def get_repart_comp():
  return repartition_Comp

def reset_repart():
  repartition_lambda[:] = []
  std_logit[:] = []
  List_composante[:] = []
  repartition_Comp[:] = []

List_composante = []
repartition_Comp = []

def get_composante():
  return List_composante

def gauche(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,:,:-n] = input1[:,:,n:]
  else:
    output = input1
  return output
def droite(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,:,n:] = input1[:,:,:-n]
  else:
    output = input1
  return output

def haut(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,:-n,:] = input1[:,n:,:]
  else:
    output = input1
  return output
def bas(n,input1):
  output = torch.zeros(input1.shape,device = 'cuda')
  if n>0:
    output[:,n:,:] = input1[:,:-n,:]
  else:
    output = input1
  return output

###################################################
#Création du réseau
###################################################

# couche custom

def use_cp_prox_tv(args,num,img,Lambda,output,List_Comp,List_Gtv,nb_proc=1):#calcule cp_prox_tv sur l'image et l'enregistre a l'indice donné
  if args.reg_sousgrad:
    Comp,rX,Gtv = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda).cpu().detach().numpy(),verbose=0,compute_Subgrads = True, max_num_threads=nb_proc) #.cpu.detach.numpy pour récupérer une valeur utilisable
    List_Gtv[num]=torch.from_numpy(Gtv).cuda()
  else:
    Comp,rX = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda).cpu().detach().numpy(),verbose=0, max_num_threads=nb_proc) #.cpu.detach.numpy pour récupérer une valeur utilisable
  output[num,0,:,:]=torch.from_numpy(rX[Comp].reshape([256,256]))#reviens a la forme d'origine et reviens en torch (transfert vers cuda)
  List_Comp[num]=Comp.astype("int64")# conserve Comp pour backward

class cut_pursuit(Function):
  @staticmethod
  def forward(ctx, args, input1, Lambda, epoch):
    # Initialisation
    batch_img = (input1[:,0,:,:]-input1[:,1,:,:]) #calcule de la diférence entre les deux classes
    output = torch.zeros(input1.shape)
    List_Comp=[np.arange(input1.shape[2]*input1.shape[3])]*input1.shape[0]
    if args.reg_sousgrad:
      List_Gtv=torch.zeros(Lambda.shape, device = 'cuda')
    else:
      List_Gtv = 0
    list_process = []

    # image a régularisé
    reg = ((Lambda[:,int((8/10)*Lambda.shape[1])] > ((1e-3)*torch.std(batch_img,unbiased=False,dim=[1,2]))) * (epoch >= args.blocage_epoch))
    index_reg = torch.arange(0,batch_img.shape[0],1)[reg]
    index_not_reg = torch.arange(0,batch_img.shape[0],1)[torch.logical_not(reg)]

    # Calcul
    assert mp.cpu_count() > input1.shape[0] # le batch ne dois pas dépassé le nombre de coeur du processeur pour la paralélisation
    
    for index_batch in index_reg:
      list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,batch_img[index_batch,:,:].cpu().detach().numpy().transpose(),Lambda[index_batch],output,List_Comp,List_Gtv))) #math.floor(mp.cpu_count()/input1.shape[0])
      list_process[len(list_process)-1].start()
    for p in list_process:
      p.join() 

    List_Comp = torch.tensor(np.array(List_Comp),device = "cuda",dtype=torch.int64)

    args.compte_composante.append(torch.mean(torch.max(List_Comp,dim = 1)[0].float()))
    if input1.shape[0] == 1:
      List_composante.append(torch.max(List_Comp,dim = 1)[0])
    
    List_Comp = List_Comp.reshape([input1.shape[0],input1.shape[2],input1.shape[3]])
    
    if input1.shape[0] == 1:
      repartition_Comp.append(List_Comp.detach())

    output = output.cuda()
    output[index_not_reg,0,:,:] = batch_img[index_not_reg]# pas de régularisation => input = output
    if args.reg_sousgrad:
      List_Gtv[index_not_reg] = Lambda[index_not_reg]

    ctx.batch_img = batch_img.detach()
    ctx.output = copy.deepcopy(output[:,0,:,:]) # séparation de output et ctx.output pour evité la perte memoire
    ctx.Lambda = Lambda.detach()
    ctx.args = args
    # ctx.index_reg = index_reg #regularisation faite
    # ctx.index_not_reg = index_not_reg
    ctx.epoch = epoch # numéro d'epoch pour l'importance de l'aprentisage de lambda
    
    return output,List_Gtv
  
  @staticmethod
  @once_differentiable
  def backward(ctx, grad_output,grad_sousgrad):
    batch_img = ctx.batch_img
    output = ctx.output
    Lambda = ctx.Lambda
    args = ctx.args
    # index_reg = ctx.index_reg #regularisation faite
    # index_not_reg = ctx.index_not_reg
    epoch = ctx.epoch # numéro d'epoch pour l'importance de l'aprentisage de lambda

    grad_input = torch.zeros(grad_output.shape,device = 'cuda')
    List_Comp=[np.arange(grad_output.shape[2]*grad_output.shape[3])]*grad_output.shape[0]
    if args.reg_sousgrad:
      List_Gtv=torch.zeros(Lambda.shape, device = 'cuda')
    else:
      List_Gtv = 0
    list_process = []

    output_d = torch.zeros(grad_output.shape,device = 'cuda')
    grad_output = grad_output[:,0,:,:]
    grad_output_squeeze = grad_output.reshape([grad_output.shape[0],grad_output.shape[1]*grad_output.shape[2]])

    batch_img_d = batch_img + args.delta * grad_output
    output_d[:,0] = batch_img_d 
    Lambda_d = Lambda + args.delta * (grad_output_squeeze[:,args.edges[:,1]]+grad_output_squeeze[:,args.edges[:,0]])
    
    reg = ((Lambda_d[:,int((8/10)*Lambda_d.shape[1])] > ((1e-3)*torch.std(batch_img_d,unbiased=False,dim=[1,2]))) * (epoch >= args.blocage_epoch))
    index_reg = torch.arange(0,batch_img_d.shape[0],1)[reg]
    index_not_reg = torch.arange(0,batch_img_d.shape[0],1)[torch.logical_not(reg)]

    for index_batch in index_reg:
      list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,batch_img_d[index_batch,:,:].cpu().numpy().transpose(),Lambda_d[index_batch],output_d,List_Comp,List_Gtv))) #math.floor(mp.cpu_count()/input1.shape[0])
      list_process[len(list_process)-1].start()
    for p in list_process:
      p.join()
    
    grad_input[:,0,:,:] = -(1/args.delta) * (output - output_d[:,0])
    grad_input[:,1,:,:] = -grad_input[:,0,:,:]

    output = output.reshape([output.shape[0],output.shape[1]*output.shape[2]])
    # output_d = torch.zeros(output_d.shape, device = 'cuda')
    # for index_batch in index_reg:
    #   list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,batch_img[index_batch,:,:].cpu().numpy().transpose(),Lambda_d[index_batch],output_d,List_Comp,List_Gtv))) #math.floor(mp.cpu_count()/input1.shape[0])
    #   list_process[len(list_process)-1].start()
    # for p in list_process:
    #   p.join()
    
    output_d = output_d[:,0].reshape([output_d.shape[0],output_d.shape[2]*output_d.shape[3]])
    batch_img = batch_img.reshape([batch_img.shape[0],batch_img.shape[1]*batch_img.shape[2]])

    grad_lambda = torch.zeros([grad_output.shape[0],args.edges.shape[0]],device = 'cuda')
    grad_lambda = -(1/args.delta) * ((output[:,args.edges[:,1]] - output_d[:,args.edges[:,1]]) + (output[:,args.edges[:,0]] - output_d[:,args.edges[:,0]]))
    
    # blokage de l'aprentissage de lambda sur les premières époch
    if epoch < args.blocage_epoch:
      grad_lambda = grad_lambda*0
    else:
      if  epoch < args.blocage_epoch + args.red_grad_lambda:
        grad_lambda = grad_lambda * ((epoch - args.blocage_epoch + 1)/args.red_grad_lambda)
    
    return None,grad_input,grad_lambda,None

class Custom_layer(nn.Module):
  def __init__(self):
    super().__init__()
  
  def forward(self,args,input1,Lambda,epoch):
    return cut_pursuit.apply(args, input1, Lambda, epoch)

# réseau

def init_weights(m):
  if isinstance(m,nn.Conv1d):
    m.weight.data.fill_(1e-20)
    m.bias.data.fill_(1e-20)

class SegNet(nn.Module):
  """
  SegNet network for semantic segmentation
  """
  
  def __init__(self, args):
    """
    initialization function
    """
    super(SegNet, self).__init__() #necessary for all classes extending the module class

    self.is_cuda = args.cuda
    
    #checks that the conv widths are compatible, throws an error otherwise
    assert(args.conv_width[0] == args.conv_width[1])
    assert(args.conv_width[2]%(1/(args.nb_feature*2)) == 0)

    if hasattr(args,"dilation"):
      padding = int(((args.kernel_size+2*args.dilation-2)-1)/2)
      kernel_size = args.kernel_size
      dilation = args.dilation
    else:
      padding = 1
      kernel_size = 3
      dilation = 1
    
    self.maxpool = nn.MaxPool2d(2,2,return_indices=True) #maxpooling layer
    self.unpool = nn.MaxUnpool2d(2,2) #unpooling layer
    #encoder (remember that conv_{i0} and conv_{i1} have the same width d_i = conv_width[i]
    self.conv00 = nn.Sequential(nn.Conv2d(in_channels=args.n_channels, out_channels=args.conv_width[0] , kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[0]),nn.ReLU(True))
    self.conv01 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[0], out_channels=args.conv_width[0], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[0]),nn.ReLU(True))
    # self.drop_0 = nn.Dropout(p = coef_drop)
    self.conv10 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[0], out_channels=args.conv_width[1], kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[1]),nn.ReLU(True))
    self.conv11 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1], out_channels=args.conv_width[1], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[1]), nn.ReLU(True))
    # self.drop_1 = nn.Dropout(p = coef_drop)
    #decoder
    self.conv20=nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1] + args.conv_width[0], out_channels=args.conv_width[2], kernel_size=kernel_size, padding=1, padding_mode='reflect'),  nn.BatchNorm2d(args.conv_width[2]), nn.ReLU(True))
    self.conv21=nn.Sequential(nn.Conv2d(in_channels=args.conv_width[2], out_channels=args.conv_width[2], kernel_size=kernel_size,padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[2]), nn.ReLU(True))
    
    if args.do_reg:
      in_channels = int(args.conv_width[2]*args.nb_feature*2) # taille de l'entrée des Lambda
      if hasattr(args,"logit") and args.logit:
        in_channels = in_channels + 2*args.n_class
      if hasattr(args,"connectivities"):
        in_channels = in_channels + 1
      self.conv_Lambda = nn.Sequential(nn.Conv1d(in_channels=in_channels, out_channels=1, kernel_size = 1,  padding=0), nn.ReLU(True))
      self.conv_Lambda.apply(init_weights) # poids petit au début de l'apprentissage

      if hasattr(args,"couche_sup_lambda") and args.couche_sup_lambda:
        self.conv_Lambda_sup = nn.Sequential(nn.Conv1d(in_channels=in_channels, out_channels=in_channels, kernel_size = 1,  padding=0), nn.BatchNorm1d(in_channels),  nn.ReLU(True))

      self.convD=nn.Conv2d(in_channels=int(args.conv_width[2]*(1-args.nb_feature)), out_channels=args.n_class, kernel_size=1, padding=0)
     
      self.cust = Custom_layer()
    else:
      self.convD=nn.Conv2d(in_channels=args.conv_width[2], out_channels=args.n_class, kernel_size=1, padding=0)

    if args.cuda: #put the model on the GPU memory
      self.cuda()
    
  def forward(self, args, input, epoch):
    """
    the function called to run inference
    """ 
    if self.is_cuda: #put the data on the GPU
      input = input.cuda()
    
    #-----encoder----
    #level a
    x0 = self.conv01(self.conv00(input))
    x0down, indices_a_b = self.maxpool(x0)
    #level b
    x1=self.conv11(self.conv10(x0down))
    #-----decoder----
    #level a       
    x1up = self.unpool(x1, indices_a_b)
    x2 = self.conv21(self.conv20(torch.cat((x1up[:,:int(x1up.shape[1]/2)],x0[:,:int(x0.shape[1]/2)],x1up[:,int(x1up.shape[1]/2):],x0[:,int(x0.shape[1]/2):]),1)))

    #régularisation
    if args.do_reg:
      #organisation des donnée par arête
      x3 = self.convD(x2[:,int(x2.shape[1]*args.nb_feature):,:,:])
      x2_squeeze = x2.reshape([x2.shape[0],x2.shape[1],x2.shape[2]*x2.shape[3]])
      if hasattr(args,"logit") and args.logit: # récupération des inputs
        x3_squeeze = x3.reshape([x3.shape[0],x3.shape[1],x3.shape[2]*x3.shape[3]])
        x_Lambda = torch.cat((x2_squeeze[:,:int(x2_squeeze.shape[1]*args.nb_feature),:],x3_squeeze),1)
      else :
        x_Lambda = x2_squeeze[:,:int(x2_squeeze.shape[1]*args.nb_feature),:]
      if hasattr(args,"connectivities"):# organisation par arrete + ajout des conectivité
        feature_Lambda = torch.zeros([x_Lambda.shape[0],x_Lambda.shape[1]*2+1,args.edges.shape[0]],device = 'cuda')
        feature_Lambda[:,int(x_Lambda.shape[1]):-1,:] = torch.minimum(x_Lambda[:,:,args.edges[:,0]],x_Lambda[:,:,args.edges[:,1]])
        feature_Lambda[:,-1:,:] = args.connectivities.repeat(x_Lambda.shape[0],1).reshape(feature_Lambda[:,-1:,:].shape)
      else:
        feature_Lambda = torch.zeros(x_Lambda.shape[0],x_Lambda.shape[1]*2,args.edges.shape[0],device = 'cuda')
        feature_Lambda[:,int(x_Lambda.shape[1]):,:] = torch.minimum(x_Lambda[:,:,args.edges[:,0]],x_Lambda[:,:,args.edges[:,1]])
      feature_Lambda[:,:int(x_Lambda.shape[1]),:] = torch.maximum(x_Lambda[:,:,args.edges[:,0]],x_Lambda[:,:,args.edges[:,1]])
      
      if hasattr(args,"couche_sup_lambda") and args.couche_sup_lambda:
        Lambda = torch.squeeze(self.conv_Lambda(self.conv_Lambda_sup(feature_Lambda)),1)
      else:
        Lambda = torch.squeeze(self.conv_Lambda(feature_Lambda),1)

      if not self.training:
        repartition_lambda.append(Lambda.detach().cpu())
        std_logit.append(torch.std(x3.detach()).cpu())
      
      out,List_Gtv = self.cust(args, x3, Lambda, epoch)

      if args.reg_sousgrad:
        List_Gtv = List_Gtv.detach()
        reg_value = torch.sum(torch.square(Lambda-torch.abs(List_Gtv)))/(input.shape[0]*65536)
      else:
        reg_value = torch.sum(torch.square(Lambda))/(input.shape[0]*65536)
    else:
      out = self.convD(x2)
      reg_value = torch.tensor(0)
    
    return out,reg_value