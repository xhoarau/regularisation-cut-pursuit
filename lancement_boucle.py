import main as M
import torch

n_boucle = 10

# définition des paramètre pour le réseau
name = "grad_analytique_graph_diag_d1"
print(name)

#verifie si un réseau du même nom exist
path = "Models/"
if M.os.path.exists(path):
  filename = path + "resultat_" + name + ".pt"
  assert not M.os.path.isfile(filename)
else:
  path = M.os.path.join(M.os.getcwd(),M.os.pardir,"Models/")
  if M.os.path.exists(path):
    filename = path + "resultat_" + name + ".pt"
    assert not M.os.path.isfile(filename)
  else:
    path = M.os.getcwd()
    filename = path + "resultat_" + name + ".pt"
    assert not M.os.path.isfile(filename)

### option de calcul
Lambda_unique = False
gradient_analytique = True
graphe_tronque = False
diff_fini = False

pretrain_Lambda = False # récupère un réseau spécifique pour les Lambdas
name_save_lambda = "edge_detection_ultra_leger"
edge_detection = False # apprend un réseau spécifique pour les Lamdbas
n_dilation = 4 # nombre de convolution dans le bloque enrichissement 

reseau_complet = Lambda_unique + gradient_analytique + graphe_tronque + diff_fini
assert (Lambda_unique + gradient_analytique + graphe_tronque + diff_fini + edge_detection) == 1

### caractéristique réseau
n_epoch = 100 # nombre maxixmum d'epoch pour l'apprentissage
batch_size = 8
n_class = 2
class_names = ["background","Square"]
n_channels = 1
conv_width = [2,2,2] # attention dernier élément doit être divisible par 1/nb_feature
niveau_sup = False
conv_width_sup = [32,32]
coef_weight = 1 # coefficient reglant l'equilibrage des classes [0,1] ( Si 0 => 1/n_classe; Si 1 => equilibrage en fonction du nombre de pixels)
cuda = 1
lr = 1e-2
coef_drop = 0.5 # coeficient de toute les couche de drop out
n_epoch_validation = 10 # nombre d'époch avant arret de l'apprentissage si pas d'amelioration ( a partir de la fin de l'apprentissage des lambdas : blocage_epoch + 2*red_grad_lambda)
set_name = 'square_5_low_label_voisin' # choix des données (nom du fichier)
choix_aléatoire = False # choix des images entrainement et validation

# toute les paire de convolution avec les mêmes paramètres:
dilation = 2 # seulement la deuxième convolution de chaque paire
kernel_size = 3 # toute les convolutions

retrain = False # nouvel entrainement d'un model existant
model_name = "Lambda_unique_not_reg_batchnorm_update" # nom du model a réentrainé
blocage_reseau = False # bloque les paramètres du réseau préentrainé (apprentisage de la régularisation seul)

# scheduler s'applique sur tout les paramètres a partir du début de leur apprentisage (prise en compte de blocage_epoch et de red_grad_lambda)
scheduler_step = 10 # nb epoch entre chaque changement de lr
scheduler_gamma = 0.1 # facteur appliqué au lr

### caractéristique regularisation
do_reg = True
graph_dist = True 
List_Distance = [1,8,32]
max_dir = 1
do_use_connect_set = 0
# 0 : ne pas utilisé les donné sur les arrête
# 1 : donner les Lambdas "Parfait" (incompatible avec donné satelitaire)
# 2 : ajouté les donné dans la loss (utilisé avec coef_reg)
# 3 : donner les Lambdas "Parfait" + générer des résidu (incompatible avec donné satelitaire)
TV = False # ajoute un terme de variation total dans la loss
coef_TV = 10

# génération du graphe lier au donné:
taille_graph = [256,256] #taille spacial des données
connectivity = 2

blocage_epoch = 5 # nombre d'époch sans régularisation (10 trop grand le réseau est trop précis) 
# (-1 => aprend le modèle sans régularisation puis l'ajoute en utilisant blocage_reseau pour bloqué ou non le premier model)
reg_sousgrad = False #choix controle Ridge ou Subgrad (si coef_reg = 0 mettre False pour economie de memoire) (pris en compte si do_use_connect_set != 2)
coef_reg = 0 # poid du controle des lambda dans la loss
# petit nombre de composante => 10% des arrète coupé sois un coût sur 115000 arrète
# 94% des pixels deja bien prédis => gain possible estimé 1%
# coef reg < 1% / 115000 = 9e-8 sois environ 1e-7
blocage_reg = 0 # nombre d'epoch sans contrôle des lambdas (aprés blocage epoch)
nb_feature = 1/2 # proportion de feature donnée au lambda ([0,1])
red_grad_lambda = 10 #nombre d'epoch de reduction des gradiants de lambda et de la TV (aprés blocage epoch)
taille_sous_graph_pixel = 1 #taille du sous graph pour le calcul du gradiant (si calcul sur graph tronqué)
taille_sous_graph_arret = 0 #taille du sous graph pour le calcul du gradiant des lambdas (si calcul sur graph tronqué)
do_augment_front = False # augmenté la loss au frontière de class (mettre Faux si coef_front = 0 economie de memoire)
coef_front = 1 # coefficient reglant l'importance des pixels au frontière [0,1]
logit = False # ajoute les logits pour la generation des Lambdas
couche_sup_lambda = False # ajout d'une couche suplémentaire sur la génération des lambdas
Lambda_separe = False # la génération des lambdas a son propre réseau
conv_width_Lambda = [24,24,12] # utile si Lambda_separe = True
Lambda_init = 1 # valeur initial de lambda si Lambda unique

### caractéristique données
prop_degrad = 0 # proportion de dégradation de l'etiquetage ([0,1])
prop_img_train_set = 1 # nombre d'image dans l'ensemble d'entrainement (max * [0,1])
urban = False # choix entre des donnée urban/not urban or road/not road (mettre a faux si square = True)
square = True # choix set carré
# print("parametre : ",GPUtil.getGPUs()[0].memoryUsed)
### création des données
args = M.sc.argument(Lambda_unique,gradient_analytique,graphe_tronque,diff_fini,pretrain_Lambda,name_save_lambda,edge_detection,n_dilation, \
                  n_epoch,batch_size,n_class,class_names,n_channels,conv_width,niveau_sup,conv_width_sup,cuda,lr,coef_drop,n_epoch_validation,set_name, \
                  dilation,kernel_size,retrain,model_name,blocage_reseau,scheduler_step,scheduler_gamma, \
                  do_reg,graph_dist,List_Distance,max_dir,do_use_connect_set,TV,coef_TV,taille_graph,connectivity, \
                  blocage_epoch,reg_sousgrad,coef_reg,blocage_reg,nb_feature,red_grad_lambda,taille_sous_graph_pixel, \
                  taille_sous_graph_arret,do_augment_front,logit,couche_sup_lambda,Lambda_separe,conv_width_Lambda,Lambda_init, \
                  prop_degrad,prop_img_train_set,urban,square)
# print("args : ",GPUtil.getGPUs()[0].memoryUsed)

IoU = torch.zeros([n_boucle,3])
Loss = torch.zeros([n_boucle,3])
Loss_reg = torch.zeros([n_boucle,3])
IoU_Logit = torch.zeros([n_boucle,3])
Loss_Logit = torch.zeros([n_boucle,3])
Loss_edge = torch.zeros([n_boucle,3])
n_comp = torch.zeros([n_boucle,3])

test_set,train_set,validation_set,n_test, n_train, n_valid = M.insert.creation_set(args,validation = not choix_aléatoire,rand_choice = choix_aléatoire)
n_train = n_train + n_valid
n_valid = 0

for num in range(n_boucle):
  print("test numero : ",num)
  resultat_Lambda,resultat = M.main(args, coef_front, coef_weight, choix_aléatoire, taille_graph, reseau_complet)
  M.tf.net_def.reset_repart()
  if resultat is not None:
    cm, loss, loss_reg, loss_total = M.tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, train_set, None, test_complet = True)
    IoU[num,0] = cm.class_IoU(show = 0, return_all = True)[1]
    Loss[num,0] = loss
    Loss_reg[num,0] = loss_reg
    n_comp[num,0] = torch.mean(torch.tensor(M.tf.net_def.get_composante()).float())
    
    M.tf.net_def.reset_repart()

    if not choix_aléatoire:
      cm, loss, loss_reg, loss_total = M.tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, validation_set, None, test_complet = True)
      IoU[num,1] = cm.class_IoU(show = 0, return_all = True)[1]
      Loss[num,1] = loss
      Loss_reg[num,1] = loss_reg
      n_comp[num,1] = torch.mean(torch.tensor(M.tf.net_def.get_composante()).float())
      M.tf.net_def.reset_repart()

    cm, loss, loss_reg, loss_total = M.tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set, None, test_complet = True)
    IoU[num,2] = cm.class_IoU(show = 0, return_all = True)[1]
    Loss[num,2] = loss
    Loss_reg[num,2] = loss_reg
    n_comp[num,2] = torch.mean(torch.tensor(M.tf.net_def.get_composante()).float())
    M.tf.net_def.reset_repart()

    if args.do_reg:
      args.do_reg = False
      args.modif = True
      cm, loss, loss_reg, loss_total = M.tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, train_set, None, test_complet = True)
      IoU_Logit[num,0] = cm.class_IoU(show = 0, return_all = True)[1]
      Loss_Logit[num,0] = loss

      if not choix_aléatoire:
        cm, loss, loss_reg, loss_total = M.tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, validation_set, None, test_complet = True)
        IoU_Logit[num,1] = cm.class_IoU(show = 0, return_all = True)[1]
        Loss_Logit[num,1] = loss

      cm, loss, loss_reg, loss_total = M.tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set, None, test_complet = True)
      IoU_Logit[num,2] = cm.class_IoU(show = 0, return_all = True)[1]
      Loss_Logit[num,2] = loss
      args.do_reg = True
      args.modif = False
  
  if resultat_Lambda is not None:
    loss_Lambda = M.tf.eval_Lambda(resultat_Lambda.trained_Lambda, args, train_set)
    Loss_edge[num,0] = loss_Lambda

    if not choix_aléatoire:
      loss_Lambda = M.tf.eval_Lambda(resultat_Lambda.trained_Lambda, args, validation_set)
      Loss_edge[num,1] = loss_Lambda

    loss_Lambda = M.tf.eval_Lambda(resultat_Lambda.trained_Lambda, args, test_set)
    Loss_edge[num,2] = loss_Lambda
  
  # if num == n_boucle -1:
  path = "Models/"
  if M.os.path.exists(path):
    if reseau_complet:
      M.sr.save(resultat, path + "resultat_" + name + "_" + str(num) + ".pt")
    if args.edge_detection:
      M.sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + "_" + str(num) +  ".pt")
    M.sr.save(args, path + "args_" + name + "_" + str(num) +  ".pt")
  else:
    path = M.os.path.join(os.getcwd(),os.pardir,"Models/")
    if M.os.path.exists(path):
      if reseau_complet:
        M.sr.save(resultat, path + "resultat_" + name + "_" + str(num) +  ".pt")
      if args.edge_detection:
        M.sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + "_" + str(num) +  ".pt")
      M.sr.save(args, path + "args_" + name + "_" + str(num) +  ".pt")
    else:
      path = M.os.getcwd()
      if reseau_complet:
        M.sr.save(resultat, path + "resultat_" + name + "_" + str(num) +  ".pt")
      if args.edge_detection:
        M.sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + "_" + str(num) +  ".pt")
      M.sr.save(args, path + "args_" + name + "_" + str(num) +  ".pt")

print("mean IoU Train : ", torch.mean(IoU[:,0]).item()*100, "%")
print("mean IoU Valid : ", torch.mean(IoU[:,1]).item()*100, "%")
print("mean IoU Test : ", torch.mean(IoU[:,2]).item()*100, "%")

print("maximum IoU Test : ", torch.max(IoU[:,2]).item()*100, "%")
print("minimum IoU Test : ", torch.min(IoU[:,2]).item()*100, "%")

print("std IoU Train : ", torch.std(IoU[:,0]).item()*100)
print("std IoU Valid : ", torch.std(IoU[:,1]).item()*100)
print("std IoU Test : ", torch.std(IoU[:,2]).item()*100)

print("mean Loss Train : ", torch.mean(Loss[:,0]).item())
print("mean Loss Valid : ", torch.mean(Loss[:,1]).item())
print("mean Loss Test : ", torch.mean(Loss[:,2]).item())

print("std Loss Train : ", torch.std(Loss[:,0]).item())
print("std Loss Valid : ", torch.std(Loss[:,1]).item())
print("std Loss Test : ", torch.std(Loss[:,2]).item())

print("mean Loss reg Train : ", torch.mean(Loss_reg[:,0]).item())
print("mean Loss reg Valid : ", torch.mean(Loss_reg[:,1]).item())
print("mean Loss Test : ", torch.mean(Loss_reg[:,2]).item())

print("std Loss reg Train : ", torch.std(Loss_reg[:,0]).item())
print("std Loss reg Valid : ", torch.std(Loss_reg[:,1]).item())
print("std Loss reg Test : ", torch.std(Loss_reg[:,2]).item())

print("mean IoU not_reg Train : ", torch.mean(IoU_Logit[:,0]).item()*100, "%")
print("mean IoU not_reg Valid : ", torch.mean(IoU_Logit[:,1]).item()*100, "%")
print("mean IoU not_reg Test : ", torch.mean(IoU_Logit[:,2]).item()*100, "%")

print("std IoU not_reg Train : ", torch.std(IoU_Logit[:,0]).item()*100)
print("std IoU not_reg Valid : ", torch.std(IoU_Logit[:,1]).item()*100)
print("std IoU not_reg Test : ", torch.std(IoU_Logit[:,2]).item()*100)

print("mean Loss not_reg Train : ", torch.mean(Loss_Logit[:,0]).item())
print("mean Loss not_reg Valid : ", torch.mean(Loss_Logit[:,1]).item())
print("mean Loss not_reg Test : ", torch.mean(Loss_Logit[:,2]).item())

print("std Loss not_reg Train : ", torch.std(Loss_Logit[:,0]).item())
print("std Loss not_reg Valid : ", torch.std(Loss_Logit[:,1]).item())
print("std Loss not_reg Test : ", torch.std(Loss_Logit[:,2]).item())

print("mean Loss_edge Train : ", torch.mean(Loss_edge[:,0]).item())
print("mean Loss_edge Valid : ", torch.mean(Loss_edge[:,1]).item())
print("mean Loss_edge Test : ", torch.mean(Loss_edge[:,2]).item())

print("std Loss_edge Train : ", torch.std(Loss_edge[:,0]).item())
print("std Loss_edge Valid : ", torch.std(Loss_edge[:,1]).item())
print("std Loss_edge Test : ", torch.std(Loss_edge[:,2]).item())

print("mean n_comp Train : ", torch.mean(n_comp[:,0]).item())
print("mean n_comp Valid : ", torch.mean(n_comp[:,1]).item())
print("mean n_comp Test : ", torch.mean(n_comp[:,2]).item())

print("std n_comp Train : ", torch.std(n_comp[:,0]).item())
print("std n_comp Valid : ", torch.std(n_comp[:,1]).item())
print("std n_comp Test : ", torch.std(n_comp[:,2]).item())

M.sr.save(IoU,"Models/" + name + "_IoU.pt")
M.sr.save(Loss,"Models/" + name + "_Loss.pt")
M.sr.save(Loss_reg,"Models/" + name + "_Loss_reg.pt")
M.sr.save(IoU_Logit,"Models/" + name + "_IoU_Logit.pt")
M.sr.save(Loss_Logit,"Models/" + name + "_Loss_Logit.pt")
M.sr.save(Loss_edge,"Models/" + name + "_Loss_edge.pt")
M.sr.save(n_comp,"Models/" +   name + "_n_comp.pt")
