import modules.storage_class as sc
import modules.save_result as sr
import modules.insertion_donne as insert
import modules.train_fonctions as tf
import modules.Confusion_Matrix as CM
import modules.network_definition as net_def
import modules.plot_evol as pl_ev
import modules.plot_exemple_result as pl_re
import os
import matplotlib.pyplot as plt

import numpy as np
import torch

name = "test"
print(name)

evol = True # plot l'evolution au cours de l'apprentissage
evaluation = True # evalue le modèle sur l'ensemble choisie
evaluation_complet = True # evalue le modèle sur l'ensemble choisie etiquetage complet (si etiquetage parciel existe)
avec_reg = True # True pour concervé la régularisation, False pour la retiré
# test = True # choix de l'ensemble (test ou train) (pris en compte si validation = False)
img_fixe = True # si False : choisi aléatoirement les images à affiché

test_lambda_unique = False # pour les modèle lambda unique test des valeurs de lambda
# test sur le même ensemble que l'evaluation
Lambdas = [2,5,10,15,20,25] # liste des lambdas a tester
name_save_lambda = "Lambda_unique_multi_TV001_test"

Compare_model = False # charge un deuxième modèle pour les comparé
name_2 = "test_multi_0"
# ne pas oublier l'option -i
# resultat et resultat_2
# args et args_2

img = True # generer une image avec des exemples de resultats
n_shown = 3 #nombre d'image a traité
category = "gpe" # attention toute les cathégorie ne sont pas compatible avec tout les modèles
  # if 'c' in category : rgb color
  # if 'i' in category : infrared
  # if 'g' in category : ground truth
  # if 'p' in category : prediction
  # if 'e' in category : error
  # if 'l' in category : lambda avec plafond (5*sigma_logit)
  # if 'L' in category : lambda sans plafond
  # if 'h' in category : histogram lambda
  # if 'C' in category : Composante
L_pred = False # choix d'affichage des Lambda gt ou pred

# récupération des résultats et paramètres
path = "Models/"
if os.path.exists(path):
  args = sr.load(path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    args = sr.load(path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    args = sr.load(path + "args_" + name + ".pt")

if hasattr(args,"edge_detection") and args.edge_detection:
  resultat_Lambda = sr.load(path + "resultat_Lambda_" + name + ".pt")
  print("nb paramètre : {}".format(sum([p.numel() for p in resultat_Lambda.trained_Lambda.parameters()])))

reseau_complet = args.Lambda_unique + args.gradient_analytique + args.graphe_tronque + args.diff_fini
if reseau_complet:
  resultat = sr.load(path + "resultat_" + name + ".pt")
  print("nb paramètre : {}".format(sum([p.numel() for p in resultat.trained_model.parameters()])))

if not avec_reg and args.do_reg:
  args.do_reg = False
  args.modif = True
  name = name + "_sans_reg"
  # name = name + "_modif"
if reseau_complet:
  print("durée entrainement(s) = ",resultat.duree_entrainement)
if hasattr(args,"edge_detection") and args.edge_detection:
  print("durée entrainement(s) = ",resultat_Lambda.duree_entrainement)

if reseau_complet and hasattr(resultat,"epoch_regularisation") :
  args.epoch_regularisation = resultat.epoch_regularisation
  print("epoch regularisation = ",resultat.epoch_regularisation)

# charge ensemble d'evaluation
if evaluation or (args.Lambda_unique and test_lambda_unique):
  test_set, train_set, validation_set, n_test, n_train, n_valid = insert.creation_set(args,validation = img_fixe, rand_choice = not img_fixe)

if evaluation:
  if reseau_complet:
    # ensemble d'entrainement
    cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, train_set, None)
    print('Train -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f' % \
            (cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))
    if args.do_reg:
      List_composante = net_def.get_composante()
      print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante).float()))
      print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante).float()))
      tf.net_def.reset_repart()
    if evaluation_complet:
      cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, train_set, None, test_complet = True)
      print('Train complet -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f\n' % \
            (cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))
    

    if validation_set is not None:
      # ensemble de validation si img fixe
      cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, validation_set, None)
      print('Validation -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f' % \
              (cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))
      if args.do_reg:
        List_composante = net_def.get_composante()
        print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante).float()))
        print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante).float()))
        tf.net_def.reset_repart()
      if evaluation_complet:
        cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, validation_set, None, test_complet = True)
        print('Validation complet -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f\n' % \
              (cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))

    # ensemble de test
    cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set, None)
    print('Test -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f' % \
            (cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))
    
    if args.do_reg:
      List_composante = net_def.get_composante()
      print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante).float()))
      print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante).float()))
      tf.net_def.reset_repart()
    if evaluation_complet:
      cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set, None, test_complet = True)
      print('Test complet -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f\n' % \
            (cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))
  
  if hasattr(args,"edge_detection") and args.edge_detection:
    loss_Lambda = tf.eval_Lambda(resultat_Lambda.trained_Lambda, args, test_set)
    print('Test -> Loss: %1.4f' % (loss_Lambda) )

if evol:
  if reseau_complet:
    pl_ev.plot_evol_IoU(resultat.evol_train_TV, "Train_" + name, resultat.epoch_save)
    pl_ev.plot_evol_IoU(resultat.evol_valid_TV, "Valid_" + name, resultat.epoch_save)
    # pl_ev.plot_evol_IoU(resultat.evol_test_TV, "test_" + name, resultat.epoch_save)
    if args.do_reg:
      pl_ev.plot_evol_coposante(resultat.evol_composante, args, name, resultat.epoch_save)
  if hasattr(args,"edge_detection") and args.edge_detection:
    pl_ev.plot_evol_Lambda(resultat_Lambda, "Lambda_" + name)

if img:
  if reseau_complet:
    pl_re.viewer(args, resultat.trained_model, resultat.epoch_save, name = name + "_train", n_shown = n_shown, category = category, train = True, L_pred=L_pred, img_fixe = img_fixe)
    pl_re.viewer(args, resultat.trained_model, resultat.epoch_save, name = name + "_test", n_shown = n_shown, category = category, train = False, L_pred=L_pred, img_fixe = img_fixe)
  if args.edge_detection:
    pl_re.viewer_Lambda(args, resultat_Lambda.trained_Lambda, name = name + "_train", n_shown = n_shown, train = True, img_fixe = img_fixe)
    pl_re.viewer_Lambda(args, resultat_Lambda.trained_Lambda, name = name + "_test", n_shown = n_shown, train = False, img_fixe = img_fixe)

if args.Lambda_unique :
  print("lambda value = %1.10f" % resultat.trained_model.get_lambda(args))
  if test_lambda_unique:
    if not args.do_reg :
      args.do_reg = True
    modif = not args.do_reg
    result = np.zeros([len(Lambdas),5])
    # i=0
    for L in Lambdas:
      # result[i,0] = L
      state_dict = resultat.trained_model.reseau_Lambda.state_dict()
      state_dict['Lambda'] = torch.tensor(L)
      resultat.trained_model.reseau_Lambda.load_state_dict(state_dict)
      print(resultat.trained_model.get_lambda(args))
      cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, train_set, None, test_complet = False)
      print('Test %3.2f -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.10f' % \
              (L, cm.overall_accuracy()*100, cm.class_IoU(), loss))
      # result[i,1] = cm.class_IoU()
      # result[i,2] = loss
      List_composante = net_def.get_composante()
      print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante[len(List_composante)-100:]).float()))
      print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante[len(List_composante)-100:]).float()))
      # result[i,3] = torch.mean(torch.tensor(List_composante[len(List_composante)-100:]).float())
      # result[i,4] = torch.std(torch.tensor(List_composante[len(List_composante)-100:]).float())
      # i=i+1
    if modif:
      args.do_reg = False
    # sr.save(result,name_save_lambda)

if Compare_model:
  print(name_2)
  path = "Models/"
  if os.path.exists(path):
    resultat_2 = sr.load(path + "resultat_" + name_2 + ".pt")
    args_2 = sr.load(path + "args_" + name_2 + ".pt")
  else:
    path = os.path.join(os.getcwd(),os.pardir,"Models/")
    if os.path.exists(path):
      resultat_2 = sr.load(path + "resultat_" + name_2 + ".pt")
      args_2 = sr.load(path + "args_" + name_2 + ".pt")
    else:
      path = os.getcwd()
      resultat_2 = sr.load(path + "resultat_" + name_2 + ".pt")
      args_2 = sr.load(path + "args_" + name_2 + ".pt")


# if train:
#   tile, gt, Lambda = train_set.__getitem__(tile_index)
# else:
#   tile, gt, Lambda = test_set.__getitem__(tile_index)
# model.eval()
# pred = model(args, tile[None,:,:,:].cuda(), Lambda, epoch_save)[0].cpu().argmax(1).squeeze()

def print_img(img,num):
  fig = plt.figure(figsize=(5, 5)) #adapted dimension
  ax = fig.add_subplot(1, 1, 1, aspect='equal')
  plt.imshow(torch.squeeze(img).detach().numpy(),cmap='gray')
  plt.savefig("test_filtre " + str(num) + ".png")
  plt.close() 

import inspect
def parametre(args):
  for i in inspect.getmembers(args):
      
    # to remove private and protected
    # functions
    if not i[0].startswith('_'):
          
      # To remove other methods that
      # doesnot start with a underscore
      if not inspect.ismethod(i[1]): 
        print(i)
        
