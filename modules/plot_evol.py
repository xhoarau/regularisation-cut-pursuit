import torch
import numpy as np
import matplotlib.pyplot as plt

def plot_evol_IoU(evol,name,epoch_valid):
  fig = plt.figure(figsize=(20,10))
  #fig.add_axes([0.1,0.1,0.5,0.5])
  subplot_index=1

  ax = fig.add_subplot(1, 2, subplot_index)
  ax.set(title='accuracy')
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),1]*100,label="overall_accuracy")
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),2],label="class_IoU")
  ax.scatter(epoch_valid,evol[epoch_valid,2])
  ax.legend(loc='lower right')
  ax.set_xlabel('i_epoch')
  ax.set_ylabel('per cent accuracy')
  ax.set_xlim(0,max(evol[:,0]))
  ax.set_ylim(0,100)
  subplot_index=subplot_index+1

  ax = fig.add_subplot(1, 2, subplot_index)
  ax.set(title='loss')
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),3],label="loss")
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),4],label="loss_reg")
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),5],label="total_loss")
  ax.legend(loc='lower right')
  ax.set_xlabel('i_epoch')
  ax.set_ylabel('loss')
  ax.set_xlim(0,max(evol[:,0]))
  ax.set_ylim(0,max(evol[:,5]))
  subplot_index=subplot_index+1
  plt.savefig("evol_IoU_" + name + ".png")
  plt.close()

def plot_evol_Lambda(result,name):
  fig = plt.figure(figsize=(10,10))
  #fig.add_axes([0.1,0.1,0.5,0.5])
  subplot_index=1
  ax = fig.add_subplot(1, 1, subplot_index)
  ax.set(title='loss')
  ax.plot(result.evol_train_Lambda[range(0,int(max(result.evol_train_Lambda[:,0])+1)),0],result.evol_train_Lambda[range(0,int(max(result.evol_train_Lambda[:,0])+1)),1],label="loss_train")
  ax.plot(result.evol_test_Lambda[range(0,int(max(result.evol_train_Lambda[:,0])+1)),0],result.evol_test_Lambda[range(0,int(max(result.evol_train_Lambda[:,0])+1)),1],label="loss_test")
  ax.plot(result.evol_valid_Lambda[range(0,int(max(result.evol_train_Lambda[:,0])+1)),0],result.evol_valid_Lambda[range(0,int(max(result.evol_train_Lambda[:,0])+1)),1],label="loss_valid")
  ax.legend(loc='lower right')
  ax.set_xlabel('i_epoch')
  ax.set_ylabel('loss')
  ax.set_xlim(0,max(result.evol_train_Lambda[:,0]))
  ax.set_ylim(0,max(result.evol_train_Lambda[:,1]))
  subplot_index=subplot_index+1
  plt.savefig("evol_loss_" + name + ".png")
  plt.close()


def plot_evol_coposante(evol, args, name, epoch_valid):

  List_val_test = []
  indice_test = []

  for index_evol in range(0,int(max(evol[:,0])+1)):
    if evol[index_evol,3] != 0:
      List_val_test.append(evol[index_evol,3])
      indice_test.append(index_evol)

  fig = plt.figure(figsize=(20,10))
  #fig.add_axes([0.1,0.1,0.5,0.5])
  subplot_index=1

  ax = fig.add_subplot(1, 2, subplot_index)
  ax.set(title='accuracy')
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),1],label="comp_train")
  ax.plot(evol[range(0,int(max(evol[:,0])+1)),0],evol[range(0,int(max(evol[:,0])+1)),2],label="comp_valid")
  ax.scatter(indice_test,List_val_test,label="comp_test",color='g')
  ax.legend(loc='lower right')
  ax.set_xlabel('i_epoch')
  ax.set_ylabel('nb_composante')
  ax.set_xlim(0,max(evol[:,0]))
  ax.set_ylim(0,np.nanmax(evol[:,1:3]))
  subplot_index=subplot_index+1

  ax = fig.add_subplot(1, 2, subplot_index)
  ax.set(title='accuracy')
  ax.plot(evol[range(args.blocage_epoch+5,int(max(evol[:,0])+1)),0],evol[range(args.blocage_epoch+5,int(max(evol[:,0])+1)),1],label="comp_train")
  ax.plot(evol[range(args.blocage_epoch+5,int(max(evol[:,0])+1)),0],evol[range(args.blocage_epoch+5,int(max(evol[:,0])+1)),2],label="comp_valid")
  ax.scatter(indice_test[len(indice_test)-3:],List_val_test[len(indice_test)-3:],label="comp_test",color='g')
  ax.legend(loc='lower right')
  ax.set_xlabel('i_epoch')
  ax.set_ylabel('nb_composante')
  ax.set_xlim(args.blocage_epoch+5,max(evol[:,0]))
  ax.set_ylim(0,np.max(evol[args.blocage_epoch+5:,1:3]))
  subplot_index=subplot_index+1
  
  plt.savefig("evol_Comp_" + name + ".png")
  plt.close()

  print("Moyenne composante sur l'ensemble de test : ",evol[int(max(evol[:,0])),3])