import numpy as np
import torch
import torch.nn as nn
from torch.autograd.function import once_differentiable
from torch_scatter import scatter_mean
from torch.autograd import Function
import copy
import sys,os
import time
import GPUtil



import modules.shift_tensor as shift

sys.path.append(os.path.join(os.getcwd(),"parallel-cut-pursuit/python/wrappers"))
sys.path.append(os.path.join(os.getcwd(),os.pardir,"parallel-cut-pursuit/python/wrappers"))
from cp_prox_tv import cp_prox_tv

import multiprocessing as mp
import threading as th

repartition_lambda = 0
std_logit = 0
List_composante = []
repartition_Comp = 0

input_bug = [0]*16

def bug():
  return input_bug

def get_repart():
  return repartition_lambda,std_logit

def get_repart_comp():
  return repartition_Comp

def reset_repart():
  repartition_lambda = 0
  std_logit = 0
  List_composante[:] = []
  repartition_Comp = 0

def get_composante():
  return List_composante

mes_temps_cut_pursuit = []
mes_temps_backward = []
mes_temps_reg_value = []
mes_temps_gene_Lambda = []
mes_temps_gene_logit = []
mes_temps_reg = []


def get_temps():
  return mes_temps_cut_pursuit,mes_temps_backward,mes_temps_reg_value,mes_temps_gene_Lambda,mes_temps_gene_logit,mes_temps_reg


###################################################
#Création du réseau
###################################################
def grad_graphe_tronque(grad_output,grad_input,grad_lambda,taille_graph_p,taille_graph_a,output,List_Comp,edges):
  mes_temps_backward.append(time.time())
  taille_comp = torch.ones(List_Comp.shape,device = 'cuda') # Compte la taille de la composante sur le graphe tronqué
  grad_output_squeeze = grad_output[:,0,:,:].reshape(grad_output.shape[0],grad_output.shape[2]*grad_output.shape[3])
  signe_arret_out = torch.sign(output[:,edges[:,0]]-output[:,edges[:,1]]) # signe des différance de part au d'autre de chaque arrete

  # # calcul grad input
  
  for hor in range(-taille_graph_p,taille_graph_p+1):
    for ver in range(-taille_graph_p+abs(hor),taille_graph_p-abs(hor)+1):
      if ver !=0 or hor !=0:
        comp_temp = List_Comp.detach()
        grad_temp = grad_output[:,0,:,:].detach()
        if hor>0:
          comp_temp = shift.droite(hor,comp_temp) # shift des tenseur 2D de "hor" pixels vers la droite (première colone mis a 0)
          grad_temp = shift.droite(hor,grad_temp)
        if hor<0:
          comp_temp = shift.gauche(-hor,comp_temp)
          grad_temp = shift.gauche(-hor,grad_temp)
        if ver>0:
          comp_temp = shift.haut(ver,comp_temp)
          grad_temp = shift.haut(ver,grad_temp)
        if ver<0:
          comp_temp = shift.bas(-ver,comp_temp)
          grad_temp = shift.bas(-ver,grad_temp)
        grad_input[:,0,:,:]= (grad_input[:,0,:,:] * taille_comp + grad_temp * ((List_Comp==comp_temp)*1))/(taille_comp+((List_Comp==comp_temp)*1)) # calcule de la moyenne a chaque étape
        taille_comp = taille_comp + (List_Comp==comp_temp)*1
  grad_input[:,1,:,:] = -grad_input[:,0,:,:]

  # # calcul grad_lambda
  # if taille_graph_a != 0:
  if taille_graph_p != taille_graph_a: # racalcul si graphe differants
    taille_comp = torch.zeros(List_Comp.shape,device = 'cuda')
    for hor in range(-taille_graph_a,taille_graph_a+1):
      for ver in range(-taille_graph_a+abs(hor),taille_graph_a-abs(hor)+1):
        comp_temp = List_Comp.detach()
        if hor>0:
          comp_temp = shift.droite(hor,comp_temp)
        if hor<0:
          comp_temp = shift.gauche(-hor,comp_temp)
        if ver>0:
          comp_temp = shift.haut(ver,comp_temp)
        if ver<0:
          comp_temp = shift.bas(-ver,comp_temp)
        taille_comp = taille_comp + (List_Comp==comp_temp)*1
  taille_comp_squeeze = taille_comp.reshape(grad_output_squeeze.shape)
  grad_lambda = (grad_output_squeeze[:,edges[:,1]]/taille_comp_squeeze[:,edges[:,1]] - grad_output_squeeze[:,edges[:,0]]/taille_comp_squeeze[:,edges[:,0]])*signe_arret_out
  mes_temps_backward[len(mes_temps_backward)-1] = time.time() - mes_temps_backward[len(mes_temps_backward)-1]
  return grad_input,grad_lambda

def grad_analytique(grad_output,grad_input,grad_lambda,index_reg,index_not_reg,output,List_Comp,edges):
  mes_temps_backward.append(time.time())
  grad_output_squeeze = grad_output[:,0,:,:].reshape(grad_output.shape[0],grad_output.shape[2]*grad_output.shape[3])
  if len(index_reg) != 0:
    # # calcul grad input reg
    # calcule de la somme des gradient de chaque composente et le divise par la taille de la composante
    List_Comp_temp = List_Comp[index_reg]
    Comp_val = torch.zeros([grad_output_squeeze.shape[0],torch.max(List_Comp_temp).int().item()+1],device = 'cuda')
    Comp_val[index_reg] = scatter_mean(grad_output_squeeze[index_reg], List_Comp_temp, dim=1)
    # redistribue les valeurs des composente sour la forme de l'image
    grad_input[index_reg,0,:,:] = torch.gather(Comp_val[index_reg],1,List_Comp_temp).reshape([len(index_reg),grad_input.shape[2],grad_input.shape[3]])
    grad_input[index_reg,1,:,:] = -grad_input[index_reg,0,:,:]
    
    # # calcul grad_lambda reg
    List_taille = torch.zeros([List_Comp.shape[0],torch.max(List_Comp_temp)+1],device  = 'cuda')
    for i in index_reg:
      List_taille[i,:torch.max(List_Comp[i])+1] = torch.unique(List_Comp[i],return_counts = True)[1] # Liste des tailles des composantes 

    out_temp = output[index_reg]
    grad_s_temp = grad_output_squeeze[index_reg]
    signe_arret_out = torch.sign(out_temp[:,edges[:,0]]-out_temp[:,edges[:,1]])
    grad_lambda[index_reg] = signe_arret_out * ((grad_s_temp[:,edges[:,1]] / torch.gather(List_taille[index_reg], 1, List_Comp_temp[:,edges[:,1]])) - (grad_s_temp[:,edges[:,0]] / torch.gather(List_taille[index_reg], 1, List_Comp_temp[:,edges[:,0]])))

  if len(index_not_reg) != 0:
    # # calcul grad input not reg
    grad_input[index_not_reg,0,:,:] = grad_output[index_not_reg,0,:,:] # pas de regularisation => gradin = gradout
    grad_input[index_not_reg,1,:,:] = -grad_input[index_not_reg,0,:,:]
    
    # # calcul grad_lambda not reg
    out_temp = output[index_not_reg]
    grad_s_temp = grad_output_squeeze[index_not_reg]
    signe_arret_out = torch.sign(out_temp[:,edges[:,0]]-out_temp[:,edges[:,1]])
    grad_lambda[index_not_reg] = signe_arret_out * (grad_s_temp[:,edges[:,1]] - grad_s_temp[:,edges[:,0]])
  mes_temps_backward[len(mes_temps_backward)-1] = time.time() - mes_temps_backward[len(mes_temps_backward)-1]
  return grad_input,grad_lambda
    
def grad_diff_fini(args, grad_output, output, grad_input, input1, Lambda, grad_lambda, epoch):
  mes_temps_backward.append(time.time())
  List_Comp=[np.arange(grad_output.shape[2]*grad_output.shape[3])]*grad_output.shape[0]
  if args.reg_sousgrad:
    List_Gtv=torch.zeros(Lambda.shape, device = 'cuda')
  else:
    List_Gtv = 0
  list_process = []

  output_d = torch.zeros(output.shape,device = 'cuda')
  grad_output = grad_output[:,0,:,:]
  grad_output_squeeze = grad_output.reshape([grad_output.shape[0],grad_output.shape[1]*grad_output.shape[2]])

  input1_d = input1 + args.delta * grad_output
  output_d[:,0] = input1_d
  Lambda_d = Lambda + args.delta * (grad_output_squeeze[:,args.edges[:,1]]+grad_output_squeeze[:,args.edges[:,0]])
  
  reg = ((Lambda_d[:,int((8/10)*Lambda_d.shape[1])] > ((1e-3)*torch.std(input1_d,unbiased=False,dim=[1,2]))) * (epoch >= args.blocage_epoch))
  index_reg = torch.arange(0,input1_d.shape[0],1)[reg]
  index_not_reg = torch.arange(0,input1_d.shape[0],1)[torch.logical_not(reg)]

  for index_batch in index_reg:
    list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,input1_d[index_batch,:,:].cpu().numpy().transpose(),Lambda_d[index_batch],output_d,List_Comp,List_Gtv))) #math.floor(mp.cpu_count()/input1.shape[0])
    list_process[len(list_process)-1].start()
  for p in list_process:
    p.join()
  grad_input[:,0,:,:] = -(1/args.delta) * (output[:,0] - output_d[:,0])
  grad_input[:,1,:,:] = -grad_input[:,0,:,:]

  output = output[:,0].reshape([output.shape[0],output.shape[2]*output.shape[3]])
  # output_d = torch.zeros(output_d.shape, device = 'cuda')
  # for index_batch in index_reg:
  #   list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,batch_img[index_batch,:,:].cpu().numpy().transpose(),Lambda_d[index_batch],output_d,List_Comp,List_Gtv))) #math.floor(mp.cpu_count()/input1.shape[0])
  #   list_process[len(list_process)-1].start()
  # for p in list_process:
  #   p.join()
  
  output_d = output_d[:,0].reshape([output_d.shape[0],output_d.shape[2]*output_d.shape[3]])
  input1 = input1.reshape([input1.shape[0],input1.shape[1]*input1.shape[2]])

  grad_lambda = -(1/args.delta) * ((output[:,args.edges[:,1]] - output_d[:,args.edges[:,1]]) + (output[:,args.edges[:,0]] - output_d[:,args.edges[:,0]]))
  mes_temps_backward[len(mes_temps_backward)-1] = time.time() - mes_temps_backward[len(mes_temps_backward)-1]
  return grad_input,grad_lambda
  

# couche custom

def use_cp_prox_tv(args,num,img,Lambda,output,List_Comp,List_Gtv,nb_proc=1):#calcule cp_prox_tv sur l'image et l'enregistre a l'indice donné
  if args.reg_sousgrad:
    # mes_temps_cut_pursuit.append(time.time())
    Comp,rX,Gtv = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda).cpu().detach().numpy(),verbose=0,compute_Subgrads = True, max_num_threads=nb_proc, pfdr_dif_tol=0.00001) #.cpu.detach.numpy pour récupérer une valeur utilisable
    # mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1] = time.time() - mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1]
    List_Gtv[num]=torch.from_numpy(Gtv).cuda()
  else:
    # mes_temps_cut_pursuit.append(time.time())
    input_bug[num] = [img, args.first_edges, args.adj_vertices, (Lambda).cpu().detach().numpy(), 0, 1, 0.00001]
    Comp,rX = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda).cpu().detach().numpy(),verbose=0, max_num_threads=nb_proc, pfdr_dif_tol=0.00001) #.cpu.detach.numpy pour récupérer une valeur utilisable
    # mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1] = time.time() - mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1]
  output[num,0,:,:]=torch.from_numpy(rX[Comp].reshape([256,256]))#reviens a la forme d'origine et reviens en torch (transfert vers cuda)
  List_Comp[num]=Comp.astype("int64")# conserve Comp pour backward

class cut_pursuit(Function):
  @staticmethod
  def forward(ctx, args, input1, Lambda, epoch):
    # Initialisation
    batch_img = (input1[:,0,:,:]-input1[:,1,:,:]) #calcule de la diférence entre les deux classes
    output = torch.zeros(input1.shape)
    List_Comp=[np.arange(input1.shape[2]*input1.shape[3])]*input1.shape[0]
    if args.reg_sousgrad:
      List_Gtv=torch.zeros(Lambda.shape, device = 'cuda')
    else:
      List_Gtv = torch.tensor(0)
    list_process = []

    # image a régularisé
    reg = ((Lambda.sort(dim = 1)[0][:,int((8/10)*Lambda.shape[1])] > ((1e-3)*torch.std(batch_img,unbiased=False,dim=[1,2]))) * (epoch >= args.blocage_epoch)).detach()
    index_reg = torch.arange(0,batch_img.shape[0],1)[reg]
    index_not_reg = torch.arange(0,batch_img.shape[0],1)[torch.logical_not(reg)]
    
    # Calcul
    assert mp.cpu_count() > input1.shape[0] # le batch ne dois pas dépassé le nombre de coeur du processeur pour la paralélisation

    mes_temps_cut_pursuit.append(time.time())

    for index_batch in index_reg:
      list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,batch_img[index_batch,:,:].cpu().detach().numpy().transpose(),Lambda[index_batch],output,List_Comp,List_Gtv))) #math.floor(mp.cpu_count()/input1.shape[0])
      list_process[len(list_process)-1].start()
    for p in list_process:
      p.join()

    mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1] = time.time() - mes_temps_cut_pursuit[len(mes_temps_cut_pursuit)-1]

    List_Comp = torch.tensor(np.array(List_Comp),device = "cuda",dtype=torch.int64).detach()

    args.compte_composante.append(torch.mean(torch.max(List_Comp.detach(),dim = 1)[0].float()))
    if input1.shape[0] == 1:
      global List_composante
      global repartition_Comp
      List_composante.append(copy.deepcopy(torch.max(List_Comp.detach().cpu(),dim = 1)[0]))
      repartition_Comp = copy.deepcopy(List_Comp.detach().cpu().reshape([input1.shape[0],input1.shape[2],input1.shape[3]]))
    
    output = output.cuda()
    output[index_not_reg,0,:,:] = batch_img[index_not_reg]# pas de régularisation => input = output

    if args.reg_sousgrad:
      List_Gtv[index_not_reg] = Lambda[index_not_reg]

    ctx.component_List_Comp = List_Comp
    ctx.index_reg = index_reg #regularisation faite
    ctx.index_not_reg = index_not_reg #regularisation faite
    ctx.output = copy.deepcopy(output) 
    # ctx.component_List_Comp = copy.deepcopy(List_Comp.detach())
    # ctx.index_reg = copy.deepcopy(index_reg.detach()) #regularisation faite
    # ctx.index_not_reg = copy.deepcopy(index_not_reg.detach()) #regularisation faite
    # ctx.output = copy.deepcopy(output.detach())# séparation de output et ctx.output pour evité la perte memoire
    ctx.args = args
    if args.diff_fini:
      ctx.input1 = copy.deepcopy(batch_img.detach())
      ctx.Lambda = copy.deepcopy(Lambda.detach())
      ctx.epoch = epoch
    
    return output,List_Gtv
  
  @staticmethod
  @once_differentiable
  def backward(ctx, grad_output,grad_sousgrad):
    
    #récuperation
    List_Comp = ctx.component_List_Comp
    index_reg = ctx.index_reg #regularisation faite
    index_not_reg = ctx.index_not_reg #regularisation non faite
    output = ctx.output.detach()
    args = ctx.args

    #initialisation
    grad_lambda = torch.zeros([grad_output.shape[0],args.edges.shape[0]],device = 'cuda')
    if not args.diff_fini:
      output = output[:,0,:,:].reshape([output.shape[0],output.shape[2]*output.shape[3]])
    else:
      input1 = ctx.input1
      Lambda = ctx.Lambda
      epoch = ctx.epoch

    if args.graphe_tronque:
      List_Comp = List_Comp.reshape([grad_output.shape[0],grad_output.shape[2],grad_output.shape[3]])+1 # +1 numerotation de 1 a n et non de 0 a n-1 pour le shift
      grad_input = grad_output.clone() # point de départ de la moyenne par étape
    else:
      grad_input = torch.zeros(grad_output.shape,device='cuda')
    
    #calcul
    if args.graphe_tronque:
      grad_input,grad_lambda = grad_graphe_tronque(grad_output,grad_input,grad_lambda,args.taille_sous_graph_pixel,args.taille_sous_graph_arret,output,List_Comp,args.edges)
    else :
      if args.Lambda_unique or args.gradient_analytique:
        grad_input,grad_lambda = grad_analytique(grad_output,grad_input,grad_lambda,index_reg,index_not_reg,output,List_Comp,args.edges)
      else:
        if args.diff_fini:
          grad_input,grad_lambda = grad_diff_fini(args, grad_output, output, grad_input, input1, Lambda, grad_lambda, epoch)
    if args.do_use_connect_set == 1:
      grad_lambda = None

    return None,grad_input,grad_lambda,None

class Custom_layer(nn.Module):
  def __init__(self):
    super().__init__()
  
  def forward(self,args,input1,Lambda,epoch):
    return cut_pursuit.apply(args, input1, Lambda, epoch)

# réseau

def init_weights(m):
  if isinstance(m,nn.Conv1d):
    m.weight.data.fill_(1e-20)
    m.bias.data.fill_(1e-20)
  if isinstance(m,nn.Conv2d):
    m.weight.data = torch.tensor([[1,0.5,0.25],[0.5,0.24,0.123],[0.12,0.36,0.15]]).repeat(m.weight.data.shape[0],m.weight.data.shape[1],1,1)
    m.bias.data.fill_(1e-20)

class Base_architecture(nn.Module):

  def __init__(self, args):
    """
    initialization function
    """
    super(Base_architecture,self).__init__() #necessary for all classes extending the module class

    self.is_cuda = args.cuda
    
    #checks that the conv widths are compatible, throws an error otherwise
    assert(args.conv_width[0] == args.conv_width[1])

    if hasattr(args,"dilation"):
      padding = int(((args.kernel_size*args.dilation-args.dilation+1)-1)/2)
      kernel_size = args.kernel_size
      dilation = args.dilation
    else:
      padding = 1
      kernel_size = 3
      dilation = 1
    
    self.maxpool = nn.MaxPool2d(2,2,return_indices=True) #maxpooling layer
    self.unpool = nn.MaxUnpool2d(2,2) #unpooling layer
    #encoder (remember that conv_{i0} and conv_{i1} have the same width d_i = conv_width[i]
    self.conv00 = nn.Sequential(nn.Conv2d(in_channels=args.n_channels, out_channels=args.conv_width[0] , kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[0]),nn.ReLU(True))
    # self.conv00.apply(init_weights)
    self.conv01 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[0], out_channels=args.conv_width[0], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[0]),nn.ReLU(True))
    # self.conv01.apply(init_weights)
    # self.drop_0 = nn.Dropout(p = coef_drop)
    if args.niveau_sup:
      self.conv_sup00 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1], out_channels=args.conv_width_sup[0], kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_sup[0]),nn.ReLU(True))
      self.conv_sup01 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_sup[0], out_channels=args.conv_width_sup[0], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_sup[0]), nn.ReLU(True))
      # self.drop_sup0 = nn.Dropout(p = coef_drop)
      self.conv_sup10 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_sup[0], out_channels=args.conv_width_sup[1], kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_sup[0]),nn.ReLU(True))
      self.conv_sup11 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_sup[1], out_channels=args.conv_width_sup[1], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_sup[0]), nn.ReLU(True))
      # self.drop_sup0 = nn.Dropout(p = coef_drop)
      in_channels = args.conv_width_sup[1] + args.conv_width_sup[0]
    else:
      in_channels = args.conv_width[0]
    
    self.conv10 = nn.Sequential(nn.Conv2d(in_channels=in_channels, out_channels=args.conv_width[1], kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[1]),nn.ReLU(True))
    # self.conv10.apply(init_weights)
    self.conv11 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1], out_channels=args.conv_width[1], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[1]), nn.ReLU(True))
    # self.conv11.apply(init_weights)
    # self.drop_1 = nn.Dropout(p = coef_drop)
    #decoder
    self.conv20=nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1] + args.conv_width[0], out_channels=args.conv_width[2], kernel_size=kernel_size, padding=1, padding_mode='reflect'),  nn.BatchNorm2d(args.conv_width[2]), nn.ReLU(True))
    # self.conv20.apply(init_weights)
    self.conv21=nn.Sequential(nn.Conv2d(in_channels=args.conv_width[2], out_channels=args.conv_width[2], kernel_size=kernel_size,padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[2]), nn.ReLU(True))
    # self.conv21.apply(init_weights)
    if (not args.do_reg) or args.Lambda_separe or args.Lambda_unique or args.edge_detection or args.pretrain_Lambda:
      self.convD=nn.Conv2d(in_channels=args.conv_width[2], out_channels=args.n_class, kernel_size=1, padding=0)
      # self.convD.weight.data.fill_(1)
      # self.convD.bias.data.fill_(1e-20)
    else:
      self.convD=nn.Conv2d(in_channels=int(args.conv_width[2]*(1-args.nb_feature)), out_channels=args.n_class, kernel_size=1, padding=0)
      # self.convD.weight.data.fill_(1)
      # self.convD.bias.data.fill_(1e-20)
      

    if args.cuda: #put the model on the GPU memory
      self.cuda()
  
  def forward(self, args, input):
    """
    the function called to run inference
    """ 

    #-----encoder----
    #level a
    
    x0 = self.conv01(self.conv00(input))
    x0down, indices_a_b = self.maxpool(x0)

    #level b
    if hasattr(args,"niveau_sup") and args.niveau_sup:
      x_sup0 = self.conv_sup01(self.conv_sup00(x0down))
      x_sup0down, indices_b_c = self.maxpool(x_sup0)
      #-----decoder----
      #level c
      x_sup1 = self.conv_sup11(self.conv_sup10(x_sup0down))

      #level b
      x_sup1up = self.unpool(x_sup1, indices_b_c)
      x1=self.conv11(self.conv10(torch.cat((x_sup1up,x_sup0),1)))
    else:
      x1=self.conv11(self.conv10(x0down))
      #-----decoder----
    
    #level a
    x1up = self.unpool(x1, indices_a_b)
    x2 = self.conv21(self.conv20(torch.cat((x1up,x0),1)))

    if (not args.do_reg and not(hasattr(args,"modif") and args.modif)) or args.Lambda_separe or args.Lambda_unique or args.edge_detection or args.pretrain_Lambda:
      logit = self.convD(x2)
    else:
      logit = self.convD(x2[:,:int(x2.shape[1]*args.nb_feature)])

    # print("fin_base : ",GPUtil.getGPUs()[0].memoryUsed)
    return logit,x2[:,int(x2.shape[1]*args.nb_feature):]
  
class reseau_Lambda(nn.Module):
  def __init__(self, args):
    """
    initialization function
    """
    super(reseau_Lambda, self).__init__() #necessary for all classes extending the module class
    
    self.is_cuda = args.cuda
    if args.Lambda_unique:
      self.Lambda = nn.Parameter(torch.tensor(args.Lambda_init).float(),requires_grad=True)
    else:
      if args.Lambda_separe:
        assert(args.conv_width_Lambda[0] == args.conv_width_Lambda[1])

        if hasattr(args,"dilation"):
          padding = int(((args.kernel_size+2*args.dilation-2)-1)/2)
          kernel_size = args.kernel_size
          dilation = args.dilation
        else:
          padding = 1
          kernel_size = 3
          dilation = 1
        
        in_channels = args.n_channels
        if hasattr(args,"logit") and args.logit:
          in_channels = in_channels + args.n_class

        self.maxpool = nn.MaxPool2d(2,2,return_indices=True) #maxpooling layer
        self.unpool = nn.MaxUnpool2d(2,2) #unpooling layer
        #encoder (remember that conv_{i0} and conv_{i1} have the same width d_i = conv_width[i]
        self.conv00 = nn.Sequential(nn.Conv2d(in_channels=in_channels, out_channels=args.conv_width_Lambda[0] , kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[0]),nn.ReLU(True))
        self.conv01 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[0], out_channels=args.conv_width_Lambda[0], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[0]),nn.ReLU(True))
        # self.drop_0 = nn.Dropout(p = coef_drop)
        self.conv10 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[0], out_channels=args.conv_width_Lambda[1], kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[1]),nn.ReLU(True))
        self.conv11 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[1], out_channels=args.conv_width_Lambda[1], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[1]), nn.ReLU(True))
        # self.drop_1 = nn.Dropout(p = coef_drop)
        #decoder
        self.conv20=nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[1] + args.conv_width_Lambda[0], out_channels=args.conv_width_Lambda[2], kernel_size=kernel_size, padding=1, padding_mode='reflect'),  nn.BatchNorm2d(args.conv_width_Lambda[2]), nn.ReLU(True))
        self.conv21=nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[2], out_channels=args.conv_width_Lambda[2], kernel_size=kernel_size,padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[2]), nn.ReLU(True))
    
        in_channels_Lambda = int(args.conv_width_Lambda[2]*2)
        if hasattr(args,"connectivities"):
          in_channels_Lambda = in_channels_Lambda + 1
      else:
        in_channels_Lambda = int(args.conv_width[2]*args.nb_feature*2) # taille de l'entrée des Lambda
        if hasattr(args,"logit") and args.logit:
          in_channels_Lambda = in_channels_Lambda + 2*args.n_class
        if hasattr(args,"connectivities"):
          in_channels_Lambda = in_channels_Lambda + 1

      self.conv_Lambda = nn.Sequential(nn.Conv1d(in_channels=in_channels_Lambda, out_channels=1, kernel_size = 1,  padding=0))
      self.conv_Lambda.apply(init_weights) # poids petit au début de l'apprentissage

      self.softplus = nn.Softplus()

      if hasattr(args,"couche_sup_lambda") and args.couche_sup_lambda:
        self.conv_Lambda_sup = nn.Sequential(nn.Conv1d(in_channels=in_channels_Lambda, out_channels=in_channels_Lambda, kernel_size = 1,  padding=0), nn.BatchNorm1d(in_channels_Lambda),  nn.ReLU(True))

      if args.cuda: #put the model on the GPU memory
        self.cuda()

  def get_lambda(self):
    if hasattr(self,"Lambda"):
      return self.Lambda
    else : 
      return None
  
  def forward(self, args, input, logit = None):
    # print("debut_Lambda : ",GPUtil.getGPUs()[0].memoryUsed)
    if args.Lambda_unique:
      return (self.Lambda/args.connectivities).repeat([input.shape[0],1])
    
    if args.Lambda_separe:
      #-----encoder----
      #level a
      # print("debut_Lambda_separe : ",GPUtil.getGPUs()[0].memoryUsed)
      input_extend = input
      if hasattr(args,"logit") and args.logit:
        assert logit is not None 
        input_extend = torch.cat((input_extend,logit),1)
      x0 = self.conv01(self.conv00(input_extend))
      x0down, indices_a_b = self.maxpool(x0)
      #level b
      x1=self.conv11(self.conv10(x0down))
      #-----decoder----
      #level a
      x1up = self.unpool(x1, indices_a_b)
      x2 = self.conv21(self.conv20(torch.cat((x1up,x0),1)))
      x_Lambda = x2.reshape([x2.shape[0],x2.shape[1],x2.shape[2]*x2.shape[3]])
      # print("fin_Lambda_separe : ",GPUtil.getGPUs()[0].memoryUsed)
    else:
      # print("debut_Lambda_classique : ",GPUtil.getGPUs()[0].memoryUsed)
      input_squeeze = input.reshape([input.shape[0],input.shape[1],input.shape[2]*input.shape[3]])
      if hasattr(args,"logit") and args.logit: # récupération des inputs
        assert logit is not None
        logit_squeeze = logit.reshape([logit.shape[0],logit.shape[1],logit.shape[2]*logit.shape[3]])
        x_Lambda = torch.cat((input_squeeze,logit_squeeze),1)
      else :
        x_Lambda = input_squeeze
      # print("fin_Lambda_classique : ",GPUtil.getGPUs()[0].memoryUsed)
    if hasattr(args,"connectivities"):# organisation par arrete + ajout des conectivité
      feature_Lambda = torch.zeros([x_Lambda.shape[0],x_Lambda.shape[1]*2+1,args.edges.shape[0]],device = 'cuda')
      feature_Lambda[:,int(x_Lambda.shape[1]):-1,:] = torch.minimum(x_Lambda[:,:,args.edges[:,0]],x_Lambda[:,:,args.edges[:,1]])
      feature_Lambda[:,-1:,:] = args.connectivities.repeat(x_Lambda.shape[0],1).reshape(feature_Lambda[:,-1:,:].shape)
    else:
      feature_Lambda = torch.zeros(x_Lambda.shape[0],x_Lambda.shape[1]*2,args.edges.shape[0],device = 'cuda')
      feature_Lambda[:,int(x_Lambda.shape[1]):,:] = torch.minimum(x_Lambda[:,:,args.edges[:,0]],x_Lambda[:,:,args.edges[:,1]])
    feature_Lambda[:,:int(x_Lambda.shape[1]),:] = torch.maximum(x_Lambda[:,:,args.edges[:,0]],x_Lambda[:,:,args.edges[:,1]])
    # print("feature_Lambda : ",GPUtil.getGPUs()[0].memoryUsed)
    if hasattr(args,"couche_sup_lambda") and args.couche_sup_lambda:
      Lambda = torch.squeeze(self.conv_Lambda(self.conv_Lambda_sup(feature_Lambda)),1)
    else:
      Lambda = torch.squeeze(self.conv_Lambda(feature_Lambda),1)

    log_temp = logit.detach()
    self.softplus.beta = ((torch.log(torch.tensor(2.))/((1e-3)*torch.std(log_temp,unbiased=False)))*2).item()
    if args.do_use_connect_set == 3:
      Lambda = self.softplus(Lambda+args.connect_set*5*torch.std(log_temp,unbiased=False))
    else:
      Lambda = self.softplus(Lambda)
    # print("fin_Lambda_classique : ",GPUtil.getGPUs()[0].memoryUsed)
    return Lambda
 

class SegNet(nn.Module):
  """
  SegNet network for semantic segmentation
  """
  
  def __init__(self, args):
    """
    initialization function
    """
    super(SegNet, self).__init__() #necessary for all classes extending the module class

    self.is_cuda = args.cuda
    self.base = Base_architecture(args)
    if args.do_reg:
      if args.pretrain_Lambda or args.edge_detection:
        self.reseau_Lambda = args.model
      else:
        if args.edge_detection:
          self.reseau_Lambda = TIN1(args)
        else:
          self.reseau_Lambda = reseau_Lambda(args)

      self.cut_pursuit = Custom_layer()

    if args.cuda: #put the model on the GPU memory
      self.cuda()
    
  def forward(self, args, input, connect_set, epoch):
    """
    the function called to run inference
    """ 

    if self.is_cuda: #put the data on the GPU
      input = input.cuda()
      connect_set = connect_set.cuda()

    if input.shape[0] != 1: # mesure temps
      mes_temps_gene_logit.append(time.time())
    
    #génération des logit
    logit,feature_Lambda = self.base(args,input)

    if input.shape[0] != 1: # mesure temps
      mes_temps_gene_logit[len(mes_temps_gene_logit)-1] = time.time() - mes_temps_gene_logit[len(mes_temps_gene_logit)-1]

    if args.step_1_end and args.do_reg and epoch >= args.blocage_epoch: # génération des Lamdbas
      if input.shape[0] != 1: # mesure temps
        mes_temps_gene_Lambda.append(time.time())
      
      if args.pretrain_Lambda or args.edge_detection:
        log_temp = logit.detach()
        Lambda = torch.einsum('a,ab->ab',(6 * torch.std(log_temp,unbiased=False,dim=[1,2,3])),torch.squeeze(self.reseau_Lambda(args, input),1))
      else:
        if args.do_use_connect_set == 1 : # utilisation des Lambda parfait
          assert not torch.any(connect_set == -1) # toute les arrète doivent être connue
          log_temp = logit.detach()

          if logit.shape[0] != connect_set.shape[0]:
            connect_set = connect_set.reshape([1,connect_set.shape[0]])
          Lambda = connect_set * (torch.std(log_temp,unbiased=False,dim=[1,2,3])*5).repeat_interleave(connect_set.shape[1]).reshape([log_temp.shape[0],connect_set.shape[1]])
        else:
          if args.Lambda_unique:
            Lambda = self.reseau_Lambda(args,input)
          else:
            if args.Lambda_separe:
              Lambda = self.reseau_Lambda(args,input,logit)
            else:
              if args.do_use_connect_set == 3: # utilisation des lambda parfait et géneration de perturbation
                assert not torch.any(connect_set == -1) # toute les arrète doivent être connue
                args.connect_set = connect_set
              Lambda = self.reseau_Lambda(args,feature_Lambda,logit)
      
      if not self.training: # recuperation des lambda en test pour l'affichage
        global repartition_lambda
        global std_logit
        repartition_lambda = copy.deepcopy(Lambda.detach().cpu())
        std_logit = copy.deepcopy(torch.std(logit.detach()).cpu())
      
      if input.shape[0] != 1: # mesure temps
        mes_temps_gene_Lambda[len(mes_temps_gene_Lambda)-1] = time.time() - mes_temps_gene_Lambda[len(mes_temps_gene_Lambda)-1]
      
      if input.shape[0] != 1: # mesure temps
        mes_temps_reg.append(time.time())
      
      # régularisation
      out,List_Gtv = self.cut_pursuit(args,logit,Lambda,epoch)

      if input.shape[0] != 1: # mesure temps
        mes_temps_reg[len(mes_temps_reg)-1] = time.time() - mes_temps_reg[len(mes_temps_reg)-1]
      
      if input.shape[0] != 1:
        mes_temps_reg_value.append(time.time())
      
      if args.do_use_connect_set == 2:
        connect_set = connect_set.reshape(Lambda.shape)
        log_temp = logit.detach()
        std_log = torch.std(log_temp,unbiased=False,dim=[1,2,3])

        reg_value = torch.sum(torch.square(torch.einsum('a,ab->ab',1/(5*std_log),((connect_set==0)*1)*Lambda))*(torch.count_nonzero(connect_set == 1)/(torch.count_nonzero(connect_set == 0)+1e-8))+torch.square(((connect_set==1)*1)*torch.maximum(torch.einsum('a,ab->ab',1/(5*std_log),torch.einsum('a,ab->ab', 5*std_log,connect_set)-Lambda),torch.zeros(Lambda.shape, device = 'cuda'))))/(input.shape[0]*(torch.count_nonzero(connect_set == 1)+torch.count_nonzero(connect_set == 0))) # torch.einsum applique le bon std a chaque image du batch
        # # Calcul de reg_value :
        # connect_set==0 => loss = lambda^2
        # connect_set==1 et Lmabda < 5* écart type des logit => loss = (5* écart type des logit - Lambda)^2
        # sinon => loss = 0
        # equilibrage empirique : nb de 1 dans connect_set / nb de 0 dans connect_set
        # somme sur tout les lambdas
      else:
        if args.reg_sousgrad:
          List_Gtv = List_Gtv.detach()
          reg_value = torch.sum(torch.square(Lambda-torch.abs(List_Gtv)))/(input.shape[0]*65536)
        else:
          reg_value = torch.sum(torch.square(Lambda))/(input.shape[0]*65536)
      
      if input.shape[0] != 1: # mesure temps
        mes_temps_reg_value[len(mes_temps_reg_value)-1] = time.time() - mes_temps_reg_value[len(mes_temps_reg_value)-1]
    else:
      if input.shape[0] != 1: # mesure temps
        mes_temps_gene_Lambda.append(time.time())
        mes_temps_gene_Lambda[len(mes_temps_gene_Lambda)-1] = time.time() - mes_temps_gene_Lambda[len(mes_temps_gene_Lambda)-1]

      out = logit

      if input.shape[0] != 1: # mesure temps
        mes_temps_reg_value.append(time.time())

      reg_value = torch.tensor(0)

      if input.shape[0] != 1: # mesure temps
        mes_temps_reg_value[len(mes_temps_reg_value)-1] = time.time() - mes_temps_reg_value[len(mes_temps_reg_value)-1]

    return out,reg_value
  
  def get_lambda(self,args):
    if args.do_reg:
      return self.reseau_Lambda.get_lambda()

class Enrichment(nn.Module):
  """
  SegNet network for semantic segmentation
  """
  
  def __init__(self, args):
    """
    initialization function
    """
    super(Enrichment, self).__init__() #necessary for all classes extending the module class
    self.is_cuda = args.cuda

    self.n_dilation = args.n_dilation

    self.conv_0 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[0], out_channels=args.conv_width_Lambda[1] , kernel_size=3, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[1]),nn.ReLU(True))

    self.conv_list = nn.ModuleList([nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[1], out_channels=args.conv_width_Lambda[1] , kernel_size=3, padding=int(((3+2*((index_conv+1)*4)-2)-1)/2), dilation = ((index_conv+1)*4), padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[1]),nn.ReLU(True)) for index_conv in range(self.n_dilation)])

    if args.cuda: #put the model on the GPU memory
      self.cuda()

  def forward(self, input):
    if self.is_cuda:
      input = input.cuda()

    X0 = self.conv_0(input)
    X = X0
    for index_conv, CONV in enumerate(self.conv_list):
      X0 = CONV(X0)
      X = X + X0
    
    return X


class TIN1(nn.Module):
  """
  SegNet network for semantic segmentation
  """
  
  def __init__(self, args):
    """
    initialization function
    """
    super(TIN1, self).__init__() #necessary for all classes extending the module class
    self.is_cuda = args.cuda

    self.conv_0 = nn.Sequential(nn.Conv2d(in_channels=args.n_channels, out_channels=args.conv_width_Lambda[0], kernel_size=3, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[0]),nn.ReLU(True))

    self.FE1 = Enrichment(args)

    self.conv_down_0 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width_Lambda[0], out_channels=args.conv_width_Lambda[0], kernel_size=3, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width_Lambda[0]),nn.ReLU(True))

    self.FE2 = Enrichment(args)

    self.conv_1 = nn.Sequential(nn.Conv1d(in_channels=args.conv_width_Lambda[1]*2+1, out_channels=args.conv_width_Lambda[2], kernel_size=1), nn.BatchNorm1d(args.conv_width_Lambda[2]),nn.ReLU(True))

    self.conv_down_1 = nn.Sequential(nn.Conv1d(in_channels=args.conv_width_Lambda[1]*2+1, out_channels=args.conv_width_Lambda[2], kernel_size=1), nn.BatchNorm1d(args.conv_width_Lambda[2]),nn.ReLU(True))

    self.conv_2 = nn.Sequential(nn.Conv1d(in_channels=args.conv_width_Lambda[2], out_channels=1, kernel_size=1), nn.BatchNorm1d(1),nn.ReLU(True))

    if args.cuda: #put the model on the GPU memory
      self.cuda()
  
  def forward(self,args,input):
    if self.is_cuda:
      input = input.cuda()
    
    x0 = self.conv_0(input)

    x0_E = self.FE1(x0).reshape([input.shape[0],args.conv_width_Lambda[1],x0.shape[2]*x0.shape[3]])

    x0_down = self.conv_down_0(x0)

    x0_down_E = self.FE2(x0_down).reshape([input.shape[0],args.conv_width_Lambda[1],x0.shape[2]*x0.shape[3]])

    x_L_1 = torch.zeros([x0_E.shape[0],x0_E.shape[1]*2+1,args.edges.shape[0]],device = 'cuda')
    x_L_1[:,:int(x0_E.shape[1]),:] = torch.maximum(x0_E[:,:,args.edges[:,0]],x0_E[:,:,args.edges[:,1]])
    x_L_1[:,int(x0_E.shape[1]):-1,:] = torch.minimum(x0_E[:,:,args.edges[:,0]],x0_E[:,:,args.edges[:,1]])
    x_L_1[:,-1:,:] = args.connectivities.repeat(x0_E.shape[0],1).reshape(x_L_1[:,-1:,:].shape)

    x_L_down_1 = torch.zeros([x0_down_E.shape[0],x0_down_E.shape[1]*2+1,args.edges.shape[0]],device = 'cuda')
    x_L_down_1[:,:int(x0_down_E.shape[1]),:] = torch.maximum(x0_down_E[:,:,args.edges[:,0]],x0_down_E[:,:,args.edges[:,1]])
    x_L_down_1[:,int(x0_down_E.shape[1]):-1,:] = torch.minimum(x0_down_E[:,:,args.edges[:,0]],x0_down_E[:,:,args.edges[:,1]])
    x_L_down_1[:,-1:,:] = args.connectivities.repeat(x0_down_E.shape[0],1).reshape(x_L_down_1[:,-1:,:].shape)

    x_L_2 = self.conv_1(x_L_1)
    x_L_down_2 = self.conv_down_1(x_L_down_1)

    x_L_3 = self.conv_2(x_L_2 + x_L_down_2)

    return x_L_3

