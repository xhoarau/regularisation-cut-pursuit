import sys,os
import numpy as np
import torch

import modules.save_result as sr
import modules.grid_graph_dist as ggd

sys.path.append(os.path.join(os.getcwd(),"grid-graph/python/bin"))
sys.path.append(os.path.join(os.getcwd(),os.pardir,"grid-graph/python/bin"))

from grid_graph import grid_to_graph
from grid_graph import edge_list_to_forward_star

class argument: #contient tout les paramètre et argument d'un réseau
  def __init__(self,Lambda_unique,gradient_analytique,graphe_tronque,diff_fini,pretrain_Lambda,name_save_lambda,edge_detection,n_dilation,n_epoch,batch_size,n_class,class_names,n_channels,conv_width,niveau_sup,conv_width_sup,cuda,lr,coef_drop,n_epoch_validation,set_name,dilation,kernel_size,retrain,model_name,blocage_reseau,scheduler_step,scheduler_gamma,do_reg,graph_dist,List_Distance,max_dir,do_use_connect_set,TV,coef_TV,taille_graph,connectivity,blocage_epoch,reg_sousgrad,coef_reg,blocage_reg,nb_feature,red_grad_lambda,taille_sous_graph_pixel,taille_sous_graph_arret,do_augment_front,logit,couche_sup_lambda,Lambda_separe,conv_width_Lambda,Lambda_init,prop_degrad,prop_img_train_set,urban,square):

    self.Lambda_unique = Lambda_unique
    self.gradient_analytique = gradient_analytique
    self.graphe_tronque = graphe_tronque
    self.diff_fini = diff_fini

    self.pretrain_Lambda = pretrain_Lambda
    self.name_save_lambda = name_save_lambda
    # if pretrain_Lambda:
    #   path = "Models/"
    #   if os.path.exists(path):
    #     resultat_Lambda = sr.load(path + "resultat_Lambda_" + name_save_lambda + ".pt")
    #   else:
    #     path = os.path.join(os.getcwd(),os.pardir,"Models/")
    #     if os.path.exists(path):
    #       resultat = sr.load(path + "resultat_Lambda_" + name_save_lambda + ".pt")
    #     else:
    #       path = os.getcwd()
    #       resultat = sr.load(path + "resultat_Lambda_" + name_save_lambda + ".pt")
    #   self.model = resultat_Lambda.trained_Lambda
    #   self.conv_width_Lambda = [self.model.conv_0[0].weight.shape[0],self.model.FE1.conv_0[0].weight.shape[0],self.model.conv_1[0].weight.shape[0]]
    self.edge_detection = edge_detection
    self.n_dilation = n_dilation

    # caractéristique réseau
    self.n_epoch = n_epoch
    self.batch_size = batch_size
    self.n_class = n_class
    self.class_names = class_names
    self.n_channels = n_channels
    self.conv_width = conv_width
    self.niveau_sup = niveau_sup
    self.conv_width_sup = conv_width_sup
    self.weight_class = [1/n_class]*n_class
    self.cuda = cuda 
    self.lr = lr
    self.coef_drop = coef_drop
    self.n_epoch_validation = n_epoch_validation
    self.set_name = set_name

    self.dilation = dilation
    self.kernel_size = kernel_size
    
    self.retrain = retrain
    # if retrain :
    #   path = "Models/"
    #   if os.path.exists(path):
    #     resultat = sr.load(path + "resultat_" + model_name + ".pt")
    #   else:
    #     path = os.path.join(os.getcwd(),os.pardir,"Models/")
    #     if os.path.exists(path):
    #       resultat = sr.load(path + "resultat_" + model_name + ".pt")
    #     else:
    #       path = os.getcwd()
    #       resultat = sr.load(path + "resultat_" + model_name + ".pt")
    #   state_dict = resultat.trained_model.cut_pursuit.state_dict()
    #   state_dict['Lambda'] = torch.tensor(Lambda_init)
    #   resultat.trained_model.cut_pursuit.load_state_dict(state_dict)
    #   self.model = resultat.trained_model
    self.model_name = model_name
    self.model = None
      
    self.blocage_reseau = blocage_reseau

    self.scheduler_step = scheduler_step
    self.scheduler_gamma = scheduler_gamma
    
    # caractéristique regularisation
    self.do_reg = do_reg
    if not Lambda_unique:
      self.do_use_connect_set = do_use_connect_set
    else:
      self.do_use_connect_set = 0
    self.TV = TV
    self.coef_TV = coef_TV
    
    if graph_dist:
      edges, connectivities = ggd.grid_to_graph_dist_diag(np.array(taille_graph,dtype='int32'),List_Distance,max_dir)
    else:
      edges, connectivities = grid_to_graph(np.array(taille_graph,dtype='int32'), connectivity = connectivity, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
    first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(taille_graph),edges)# changement de representation
    first_edges = first_edges.astype('uint32')
    adj_vertices = adj_vertices.astype('uint32')
    edges[reindex,:] = np.copy(edges)
    edges = torch.from_numpy(edges.astype("int64")).cuda()
    connectivities[reindex] = np.copy(connectivities)
    connectivities = torch.sqrt(torch.from_numpy(connectivities).cuda())
    self.edges = edges
    self.adj_vertices = adj_vertices
    self.first_edges = first_edges
    self.connectivities = connectivities

    self.blocage_epoch = blocage_epoch
    self.reg_sousgrad = reg_sousgrad
    self.coef_reg = coef_reg
    self.blocage_reg = blocage_reg
    if do_reg and not Lambda_separe and not Lambda_unique:
      self.nb_feature = nb_feature
    else:
      self.nb_feature = 1
    self.red_grad_lambda = red_grad_lambda
    self.taille_sous_graph_pixel = taille_sous_graph_pixel
    self.taille_sous_graph_arret = taille_sous_graph_arret
    self.do_augment_front = do_augment_front
    self.facteur_front = 1
    if edge_detection or pretrain_Lambda:
      self.logit = False
      self.couche_sup_lambda = False
      self.Lambda_separe = False
    else:
      self.logit = logit
      self.couche_sup_lambda = couche_sup_lambda
      self.Lambda_separe = Lambda_separe
    if not pretrain_Lambda:
      self.conv_width_Lambda = conv_width_Lambda
    self.Lambda_init = Lambda_init
    self.step_1_end = (blocage_epoch != -1) # apprentissage sans régularisation
    
    # caractéristique données
    self.prop_degrad = prop_degrad
    self.prop_img_train_set = prop_img_train_set
    self.urban = urban
    self.square = square

    self.delta = 1e13

    self.compte_composante = []

class result: #contien les resultats de l'apprentissage
  def __init__(self,trained_model,evol_train_TV,evol_valid_TV,evol_test_TV,evol_composante,duree_entrainement,epoch_save,epoch_regularisation):
    self.trained_model = trained_model
    self.evol_train_TV = evol_train_TV
    self.evol_valid_TV = evol_valid_TV
    self.evol_test_TV = evol_test_TV
    self.evol_composante = evol_composante
    self.duree_entrainement = duree_entrainement
    self.epoch_save = epoch_save
    self.epoch_regularisation = epoch_regularisation

class result_multi: #contien les resultats de l'apprentissage
  def __init__(self,List_trained_model,List_evol_train_TV,List_evol_valid_TV,List_evol_test_TV,List_evol_composante,List_temps_entrainement,List_epoch_valid,List_weight_class,List_facteur_front,List_epoch_regularisation):
    self.List_trained_model = List_trained_model
    self.List_evol_train_TV = List_evol_train_TV
    self.List_evol_valid_TV = List_evol_valid_TV
    self.List_evol_test_TV = List_evol_test_TV
    self.List_evol_composante = List_evol_composante
    self.List_temps_entrainement = List_temps_entrainement
    self.List_epoch_valid = List_epoch_valid
    self.List_weight_class = List_weight_class
    self.List_facteur_front = List_facteur_front
    self.List_epoch_regularisation = List_epoch_regularisation

class result_Lambda: #contien les resultats de l'apprentissage
  def __init__(self,trained_Lambda,evol_train_Lambda,evol_test_Lambda,evol_valid_Lambda,epoch_save_Lambda,duree_entrainement):
    self.trained_Lambda = trained_Lambda
    self.evol_train_Lambda = evol_train_Lambda
    self.evol_valid_Lambda = evol_valid_Lambda
    self.evol_test_Lambda = evol_test_Lambda
    self.duree_entrainement = duree_entrainement
