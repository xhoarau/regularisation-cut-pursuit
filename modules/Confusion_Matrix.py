from sklearn.metrics import confusion_matrix
import numpy as np

class ConfusionMatrix:
  def __init__(self, args):
    self.CM = np.zeros((args.n_class, args.n_class))
    self.n_class = args.n_class
    self.class_names = args.class_names
  
  def clear(self):
    self.CM = np.zeros((self.n_class, self.n_class))
    
  def add_batch(self, gt, pred):
    self.CM +=  confusion_matrix(gt, pred, labels = list(range(self.n_class)))
    
  def overall_accuracy(self):#percentage of correct classification
    return np.trace(self.CM)/np.sum(self.CM)

  def class_IoU(self, show = 1, return_all = False):
    ious = np.diag(self.CM)/(np.sum(self.CM,axis=0)+np.sum(self.CM,axis=1)-np.diag(self.CM))
    if show:
      print('  |  '.join('{} : {:3.2f}%'.format(name, 100*iou) for name, iou in zip(self.class_names,ious)))
    #do not count classes that are not present in the dataset in the mean IoU
    if return_all:
      result = ious
    else:
      result = 100*np.nansum(ious) / (np.logical_not(np.isnan(ious))).sum()
    return result