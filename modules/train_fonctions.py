import numpy as np
import torch
import torchnet as tnt
import torch.nn as nn
import torch.optim as optim
from tqdm import tqdm
import copy
import time
import GPUtil


import modules.Confusion_Matrix as CM
import modules.network_definition as net_def
import modules.shift_tensor as shift

mes_temps_pred = []
mes_temps_backward = []
mes_temps_optimizer = []

def get_temps():
  return mes_temps_pred,mes_temps_backward,mes_temps_optimizer

def get_repart():
  return net_def.get_repart()

def train_TV(model, optimizer, args, epoch, train_set, train_px_front = None):
  """train for one epoch"""

  model.train() #switch the model in training mode
  
  #the loader function will take care of the batching
  loader = torch.utils.data.DataLoader(train_set, \
         batch_size=args.batch_size, shuffle=True, drop_last=True)
  #tqdm will provide some nice progress bars
  loader = tqdm(loader)
    
  #will keep track of the loss
  loss_meter = tnt.meter.AverageValueMeter()
  loss_meter_reg = tnt.meter.AverageValueMeter()
  loss_meter_total = tnt.meter.AverageValueMeter()
  cm = CM.ConfusionMatrix(args)

  for index, (tiles, gt, Lambda, gt_complet) in enumerate(loader):
    if model.is_cuda: #put the ground truth on the GPU
      gt = gt.cuda()

    if epoch < args.blocage_epoch:
      optimizer[0].zero_grad() #put gradient to zero
    else:
      for o in optimizer:
        o.zero_grad()

    mes_temps_pred.append(time.time())
    pred,reg_val = model(args, tiles, Lambda, epoch) #compute the prediction
    mes_temps_pred[len(mes_temps_pred)-1] = time.time() - mes_temps_pred[len(mes_temps_pred)-1]

    if epoch < args.blocage_epoch + args.blocage_reg:
      loss_reg = torch.tensor(0.)
    else:
      loss_reg = reg_val * args.coef_reg

    if args.TV and epoch >= args.blocage_epoch:
      loss_reg = loss_reg + args.coef_TV * (min(1,(epoch-args.blocage_epoch)/args.red_grad_lambda)) * torch.mean(torch.abs(pred.reshape([pred.shape[0],pred.shape[1],pred.shape[2]*pred.shape[3]])[:,:,args.edges[:,0]] - pred.reshape([pred.shape[0],pred.shape[1],pred.shape[2]*pred.shape[3]])[:,:,args.edges[:,1]]))

    if args.do_augment_front:
      assert train_px_front is not None
      loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99, reduction = 'none') #compute the loss
      loss = loss + loss * (train_px_front[index]*(args.facteur_front-1))
      loss = torch.mean(loss)
    else:
      loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99) #compute the loss

    loss_total = loss + loss_reg

    mes_temps_backward.append(time.time())
    loss_total.backward() #compute gradients
    mes_temps_backward[len(mes_temps_backward)-1] = time.time() - mes_temps_backward[len(mes_temps_backward)-1]

    mes_temps_optimizer.append(time.time())
    if epoch < args.blocage_epoch:
      optimizer[0].step() #put gradient to zero
    else:
      for o in optimizer:
        o.step()
    mes_temps_optimizer[len(mes_temps_optimizer)-1] = time.time() - mes_temps_optimizer[len(mes_temps_optimizer)-1]

    loss_meter.add(loss.item())
    loss_meter_reg.add(loss_reg.item())
    loss_meter_total.add(loss_total.item())

    gt = gt.cpu() #back on the RAM

    labeled = np.where(gt.view(-1)!=99)[0] #select gt with a label, ie not 99
    #need to put the prediction back on the cpu and convert to numpy before feeding it to the CM
    cm.add_batch(gt.view(-1)[labeled], pred.argmax(1).view(-1)[labeled].cpu().detach().numpy())

  return cm, loss_meter.value()[0], loss_meter_reg.value()[0], loss_meter_total.value()[0]

def eval_TV(model, args, epoch, img_set, img_px_front, test_complet = False):
  """eval on test/validation set"""

  model.eval() #switch in eval mode
  
  loader = torch.utils.data.DataLoader(img_set, batch_size=1, shuffle=False, drop_last=False) 
  
  loader = tqdm(loader)
  
  loss_meter = tnt.meter.AverageValueMeter()
  loss_meter_reg = tnt.meter.AverageValueMeter()
  loss_meter_total = tnt.meter.AverageValueMeter()
  cm = CM.ConfusionMatrix(args)

  with torch.no_grad(): #do not compute gradients (saves memory)
    for index, (tiles, gt, Lambda, gt_complet) in enumerate(loader):
      if gt_complet.shape == gt.shape and test_complet:
        gt = gt_complet
      
      if model.is_cuda: #put the ground truth on the GPU
        gt = gt.cuda()
      
      pred,reg_val = model(args, tiles, Lambda, epoch) #compute the prediction

      if epoch < args.blocage_epoch+args.blocage_reg:
        loss_reg = torch.tensor(0.)
      else:
        loss_reg = reg_val * args.coef_reg

      if hasattr(args,"TV") and args.TV and epoch >= args.blocage_epoch:
        loss_reg = loss_reg + args.coef_TV * (min(1,(epoch-args.blocage_epoch)/args.red_grad_lambda)) * torch.mean(torch.abs(pred.reshape([pred.shape[0],pred.shape[1],pred.shape[2]*pred.shape[3]])[:,:,args.edges[:,0]] - pred.reshape([pred.shape[0],pred.shape[1],pred.shape[2]*pred.shape[3]])[:,:,args.edges[:,1]]))

    
      if args.do_augment_front:
        assert img_px_front is not None
        loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99, reduction = 'none') #compute the loss
        loss = loss + loss * (img_px_front[index]*(args.facteur_front-1))
        loss = torch.mean(loss)
      else:
        loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99) #compute the loss

      loss_total = loss + loss_reg
      
      loss_meter.add(loss.item())
      loss_meter_reg.add(loss_reg.item())
      loss_meter_total.add(loss_total.item())

      gt = gt.cpu() #back on the RAM

      labeled = np.where(gt.view(-1)!=99)[0] #select gt with a label, ie not 99
      #need to put the prediction back on the cpu and convert to numpy before feeding it to the CM
      cm.add_batch(gt.view(-1)[labeled], pred.argmax(1).view(-1)[labeled].cpu().detach().numpy())
    
  return cm, loss_meter.value()[0], loss_meter_reg.value()[0], loss_meter_total.value()[0]

def train_full_TV(args, train_set, validation_set, test_set, train_px_front = None, valid_px_front = None, test_px_front = None):
  """The full training loop"""
  #initialize the model
  net_def.reset_repart()
  if hasattr(args,'retrain') and args.retrain: # récupère le modèle a réentrainé
    model = args.model
  else:# créer un nouveau model
    model = net_def.SegNet(args) 

  #stock les valeurs pour les graph d'evolution
  evol_train=np.zeros((args.n_epoch,6))
  evol_valid=np.zeros((args.n_epoch,6))
  evol_test=np.zeros((5))
  evol_composante=np.zeros((args.n_epoch,4))

  print('Total number of parameters: {}'.format(sum([p.numel() for p in model.parameters()])))
  
  #define the optimizers
  #adam optimizer is always a good guess for classification
  optimizer_base = optim.Adam(model.base.parameters(), lr=args.lr) # optimiser pour le reseau générant les logits
  scheduler_base = optim.lr_scheduler.StepLR(optimizer_base, step_size=args.scheduler_step, gamma=args.scheduler_gamma)
  if args.do_reg:
    lr_Lambda = args.lr
    if args.pretrain_Lambda:
      if args.blocage_reseau:
        lr_Lambda = 0
      else:
        lr_Lambda = lr_Lambda / 10 # petit lr sur le réseau preappris
    if args.red_grad_lambda > 0:
      optimizer_Lambda = optim.Adam(model.reseau_Lambda.parameters(), lr= lr_Lambda/args.red_grad_lambda) # optimiser pour le reseau générant les lambdas
      scheduler_red_grad_Lambda =  optim.lr_scheduler.StepLR(optimizer_Lambda, step_size=1, gamma=args.red_grad_lambda**(1/args.red_grad_lambda)) # reduction de l'apprentissage des Lambdas sur red_grad_lambda première epoch
    else:
      optimizer_Lambda = optim.Adam(model.reseau_Lambda.parameters(), lr= lr_Lambda)
    scheduler_Lambda = optim.lr_scheduler.StepLR(optimizer_Lambda, step_size=args.scheduler_step, gamma=args.scheduler_gamma)
    optimizer = [optimizer_base,optimizer_Lambda]
    scheduler = [scheduler_base,scheduler_Lambda]
  else:
    optimizer = [optimizer_base]
    scheduler = [scheduler_base]
  
  if args.do_reg and not args.step_1_end and args.blocage_reseau:# met lr a zero pour bloqué l'apprentissage du reseau générant les logits
    scheduler_step_2 = optim.lr_scheduler.StepLR(optimizer_base, step_size=1, gamma=0)

  TESTCOLOR = '\033[104m'
  TRAINCOLOR = '\033[100m'
  VALIDCOLOR = '\033[101m'
  NORMALCOLOR = '\033[0m'

  #initialisation des variable de contrôle
  i_epoch = 0
  compt_verif = 0
  max_IoU = 0
  save_model = copy.deepcopy(model)
  epoch_save = 0
  epoch_regularisation = 0
  start_epoch_red_grad_Lambda = 0

  if hasattr(args,'retrain') and args.retrain: # réalise un test sur l'ancien réseau pour enregistrer un point de départ
    cm_test, loss_test, loss_reg_test, loss_total_test= eval_TV(model, args, i_epoch, test_set)
    print(TESTCOLOR)
    print('Test -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
          (cm_test.overall_accuracy()*100, cm_test.class_IoU(), loss_test, loss_reg_test, loss_total_test) + NORMALCOLOR)
    evol_test[compt_test,0]=compt_test
    evol_test[compt_test,1]=cm_test.overall_accuracy()
    evol_test[compt_test,2]=cm_test.class_IoU(show = 0)
    evol_test[compt_test,3]=loss_test
    evol_test[compt_test,4]=loss_reg_test
    evol_test[compt_test,5]=loss_total_test
    compt_test=compt_test+1

  while i_epoch < args.n_epoch and compt_verif < args.n_epoch_validation:

    #train one epoch
    net_def.reset_repart()
    cm_train, loss_train, loss_reg_train, loss_total_train = train_TV(model, optimizer, args, i_epoch - epoch_regularisation, train_set, train_px_front)

    evol_composante[i_epoch,0]=i_epoch
    evol_composante[i_epoch,1]=torch.mean(torch.tensor(args.compte_composante).float())
    args.compte_composante = []
    
    print(TRAINCOLOR)
    print('Epoch %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
          (i_epoch, cm_train.overall_accuracy()*100, cm_train.class_IoU(), loss_train,  loss_reg_train, loss_total_train) + NORMALCOLOR)
    evol_train[i_epoch,0]=i_epoch
    evol_train[i_epoch,1]=cm_train.overall_accuracy()
    evol_train[i_epoch,2]=cm_train.class_IoU(show = 0)
    evol_train[i_epoch,3]=loss_train
    evol_train[i_epoch,4]=loss_reg_train
    evol_train[i_epoch,5]=loss_total_train

    cm_valid, loss_valid, loss_reg_valid, loss_total_valid= eval_TV(model, args, i_epoch - epoch_regularisation, validation_set, valid_px_front)

    evol_composante[i_epoch,2]=torch.mean(torch.tensor(args.compte_composante).float())
    args.compte_composante = []

    print(VALIDCOLOR)
    print('Validation %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
        (i_epoch, cm_valid.overall_accuracy()*100, cm_valid.class_IoU(), loss_valid, loss_reg_valid, loss_total_valid) + NORMALCOLOR)
    evol_valid[i_epoch,0]=i_epoch
    evol_valid[i_epoch,1]=cm_valid.overall_accuracy()
    evol_valid[i_epoch,2]=cm_valid.class_IoU(show = 0)
    evol_valid[i_epoch,3]=loss_valid
    evol_valid[i_epoch,4]=loss_reg_valid
    evol_valid[i_epoch,5]=loss_total_valid

    # sauvegarde dans le cas d'un nouveau model mieux que le precedants
    if (not args.TV and (not args.do_reg or not args.step_1_end)) or i_epoch - epoch_regularisation >= args.blocage_epoch + args.red_grad_lambda + min(10, args.red_grad_lambda):
      if max_IoU <= evol_valid[i_epoch,2]:
        compt_verif = 0
        max_IoU = evol_valid[i_epoch,2]
        save_model = copy.deepcopy(model)
        epoch_save = i_epoch
        print("Nouveau Model")
      else:
        compt_verif = compt_verif +1
    
    if (args.blocage_epoch == -1 and args.step_1_end) or (args.blocage_epoch != -1 and (i_epoch >= args.blocage_epoch + max(args.blocage_reg, args.red_grad_lambda))): # si blocage_epoch != -1 => epoch_regularisation toujour = 0
      for s in scheduler:
        s.step()
    else:
      scheduler[0].step()

    if args.do_reg and (args.red_grad_lambda>0 and ((args.blocage_epoch == -1 and args.step_1_end) or (args.blocage_epoch != -1 and i_epoch >= args.blocage_epoch and i_epoch <= args.blocage_epoch + args.red_grad_lambda))):
      scheduler_red_grad_Lambda.step()

    if (i_epoch == args.n_epoch - 1) or compt_verif >= args.n_epoch_validation:
      #test fin d'un apprentissage
      
      cm_test, loss_test, loss_reg_test, loss_total_test= eval_TV(save_model, args, epoch_save, test_set, test_px_front)

      evol_composante[i_epoch,3]=torch.mean(torch.tensor(args.compte_composante).float())
      args.compte_composante = []
    
      print(TESTCOLOR)
      print('Test -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.10f Loss_total: %1.4f' % \
          (cm_test.overall_accuracy()*100, cm_test.class_IoU(), loss_test, loss_reg_test, loss_total_test) + NORMALCOLOR)
      evol_test[0]=cm_test.overall_accuracy()
      evol_test[1]=cm_test.class_IoU(show = 0)
      evol_test[2]=loss_test
      evol_test[3]=loss_reg_test
      evol_test[4]=loss_total_test
    
    if args.do_reg and compt_verif >= args.n_epoch_validation and not args.step_1_end: #si etape 1 (apprentissage sans regularisation) fini

      compt_verif = 0
      args.step_1_end = True
      epoch_regularisation = i_epoch
      model = copy.deepcopy(save_model) # repart du meilleur model
      max_IoU = 0
      if args.blocage_reseau:
        scheduler_step_2.step()
      print("régularisation")
    
    i_epoch = i_epoch + 1 
    print("\n")

  return [save_model,evol_train,evol_test,evol_valid,evol_composante,epoch_save,epoch_regularisation]

def train_Lambda(model, optimizer, args, train_set):
  model.train()

  loader = torch.utils.data.DataLoader(train_set, \
         batch_size=args.batch_size, shuffle=True, drop_last=True)
  loader = tqdm(loader)

  loss_meter = tnt.meter.AverageValueMeter()

  for index, (tiles, gt, Lambda, gt_complet) in enumerate(loader):
    if model.is_cuda: #put the ground truth on the GPU
      gt = gt.cuda()
      Lambda = Lambda.cuda()

    optimizer.zero_grad()

    pred = model(args,tiles)

    loss = torch.sum(torch.square(((Lambda==0)*1)*pred)*(torch.count_nonzero(Lambda == 1)/(torch.count_nonzero(Lambda == 0)+1e-8))+torch.square(((Lambda==1)*1)*torch.maximum(Lambda-pred,torch.zeros(Lambda.shape, device = 'cuda'))))/(torch.count_nonzero(Lambda == 1)+torch.count_nonzero(Lambda == 0))

    loss.backward()

    optimizer.step()

    loss_meter.add(loss.item())
  
  return loss_meter.value()[0]

def eval_Lambda(model, args, img_set):
  model.eval() #switch in eval mode
  
  loader = torch.utils.data.DataLoader(img_set, batch_size=1, shuffle=False, drop_last=False) 
  
  loader = tqdm(loader)

  loss_meter = tnt.meter.AverageValueMeter()

  with torch.no_grad(): #do not compute gradients (saves memory)
    for index, (tiles, gt, Lambda, gt_complet) in enumerate(loader):
      if model.is_cuda: #put the ground truth on the GPU
        gt = gt.cuda()
        Lambda = Lambda.cuda()
      
      pred = model(args,tiles)

      loss = torch.sum(torch.square(((Lambda==0)*1)*pred)*(torch.count_nonzero(Lambda == 1)/(torch.count_nonzero(Lambda == 0)+1e-8))+torch.square(((Lambda==1)*1)*torch.maximum(Lambda-pred,torch.zeros(Lambda.shape, device = 'cuda'))))/(torch.count_nonzero(Lambda == 1)+torch.count_nonzero(Lambda == 0))

      loss_meter.add(loss.item())

  return loss_meter.value()[0]      

def train_full_Lambda(args, train_set, validation_set, test_set):
  #initialize the model
  net_def.reset_repart()

  model = net_def.TIN1(args)

  print('Total number of parameters: {}'.format(sum([p.numel() for p in model.parameters()])))

  #stock les valeurs pour les graph d'evolution
  evol_train=np.zeros((args.n_epoch,2))
  evol_valid=np.zeros((args.n_epoch,2))
  evol_test=np.zeros((args.n_epoch,2))
  compt_test=0

  optimizer = optim.Adam(model.parameters(), lr= args.lr)
  scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=args.scheduler_step, gamma=args.scheduler_gamma)

  TESTCOLOR = '\033[104m'
  TRAINCOLOR = '\033[100m'
  VALIDCOLOR = '\033[101m'
  NORMALCOLOR = '\033[0m'
  
  i_epoch = 0
  compt_verif = 0
  min_loss = 1000
  save_model = copy.deepcopy(model)
  epoch_save = 0
  epoch_regularisation = 0
  start_epoch_red_grad_Lambda = 0

  while i_epoch < args.n_epoch and compt_verif < args.n_epoch_validation:
    net_def.reset_repart()
    loss_train = train_Lambda(model, optimizer, args, train_set)
    print(TRAINCOLOR)
    print('Epoch %3d -> Loss: %1.4f' % \
          (i_epoch, loss_train) + NORMALCOLOR)
    evol_train[i_epoch,0]=i_epoch
    evol_train[i_epoch,1]=loss_train

    loss_valid = eval_Lambda(model, args, validation_set)

    print(VALIDCOLOR)
    print('Validation %3d -> Loss: %1.4f' % \
        (i_epoch, loss_valid) + NORMALCOLOR)
    evol_valid[i_epoch,0]=i_epoch
    evol_valid[i_epoch,1]=loss_valid

    if min_loss > loss_valid:
      compt_verif = 0
      min_loss = loss_valid
      save_model = copy.deepcopy(model)
      epoch_save = i_epoch
      print("Nouveau Model")
    else:
      compt_verif = compt_verif +1
    
    scheduler.step()

    if (i_epoch == args.n_epoch - 1) or compt_verif >= args.n_epoch_validation:
      loss_test = eval_Lambda(save_model, args, test_set)
    
      print(TESTCOLOR)
      print('Test %3d -> Loss: %1.4f' % \
          (compt_test, loss_test) + NORMALCOLOR)
      evol_test[compt_test,0]=compt_test
      evol_test[compt_test,1]=loss_test
      compt_test=compt_test+1
    
    i_epoch = i_epoch + 1
    print('\n')
  return [save_model,evol_train,evol_test,evol_valid,epoch_save]
