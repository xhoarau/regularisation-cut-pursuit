import numpy as np

def arrete_1_D(V,dist):
  return np.array([np.arange(V-dist),np.arange(V-dist)+dist]).transpose()

def arrete(D,taille,dist):
  if D == 1 :
    return arrete_1_D(taille,dist)
  else:
    SD_1 = np.prod(taille[:D-1])
    ED_1 = arrete(D-1,taille[:D-1],dist)
    E_1 = arrete_1_D(taille[D-1],dist)*SD_1
    edges = np.add(np.repeat(ED_1,taille[D-1],axis=0),np.tile(np.arange(taille[D-1])*SD_1,ED_1.shape[0]).reshape([taille[D-1]*ED_1.shape[0],1]))
    if E_1.shape[0] != 0:
      edges = np.concatenate((edges,np.add(np.repeat(E_1,SD_1,axis=0),np.tile(np.arange(SD_1),E_1.shape[0]).reshape([SD_1*E_1.shape[0],1]))))

  return edges

def grid_to_graph_dist(D,taille,L_dist):
  assert len(taille) == D
  edges = np.empty([0,2],dtype='int64')
  connectivities = np.empty([0],dtype='int64')
  n_edge_prec = 0
  for d in L_dist:
    edges = np.concatenate((edges,arrete(D,taille,d)))
    connectivities = np.concatenate((connectivities,np.full((edges.shape[0]-n_edge_prec),d)))
    n_edge_prec = edges.shape[0]
  return edges,connectivities


def arrete_diag(taille,dist,dir):
  assert dist < taille[0]
  assert 2*dist%dir == 0
  candidate = np.concatenate((np.arange(dist+1)*taille[0]+dist,np.flip(np.arange(dist*2-1)+dist*taille[0]-(dist-1)),np.flip((np.arange(dist)+1)*taille[0]-dist)))
  indices = (np.arange(dir*2)*(2*dist/dir)).astype('int32')
  start = np.arange(taille[0]*taille[1])
  start = start[np.argwhere(np.logical_or(start%taille[0]<taille[0]-dist,start/taille[0]<taille[1]-dist))]
  edges = np.stack((start.repeat(candidate[indices].shape[0],axis=0).squeeze(),np.tile(candidate[indices],start.shape[0])+start.repeat(candidate[indices].shape[0],axis=0).squeeze()),axis=1)
  return edges[np.logical_and(edges[:,1]<taille[0]*taille[1],abs(edges[:,1]%taille[0]-edges[:,0]%taille[0])<=dist)]

def grid_to_graph_dist_diag(taille,L_dist,max_dir):
  dirs = np.arange(max_dir)+1
  edges = np.empty([0,2],dtype='int64')
  connectivities = np.empty([0],dtype='int64')
  n_edge_prec = 0
  for d in L_dist:
    assert d < taille[0]/2
    edges = np.concatenate((edges,arrete_diag(taille,d,dirs[np.max(np.argwhere(2*d%dirs == 0))])))
    connectivities = np.concatenate((connectivities,np.full((edges.shape[0]-n_edge_prec),d)))
    n_edge_prec = edges.shape[0]
  return edges,connectivities