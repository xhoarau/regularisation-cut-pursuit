import numpy as np
import torch
import time
import copy
import sys
import os
import GPUtil

import modules.insertion_donne as insert
import modules.save_result as sr
import modules.train_fonctions as tf
import modules.storage_class as sc
import modules.shift_tensor as shift

def main(args, coef_front, coef_weight, choix_aléatoire, taille_graph, reseau_complet):
  if args.pretrain_Lambda:
    path = "Models/"
    if os.path.exists(path):
      resultat_Lambda = sr.load(path + "resultat_Lambda_" + args.name_save_lambda + ".pt")
    else:
      path = os.path.join(os.getcwd(),os.pardir,"Models/")
      if os.path.exists(path):
        resultat = sr.load(path + "resultat_Lambda_" + args.name_save_lambda + ".pt")
      else:
        path = os.getcwd()
        resultat = sr.load(path + "resultat_Lambda_" + args.name_save_lambda + ".pt")
    args.model = resultat_Lambda.trained_Lambda
    args.conv_width_Lambda = [args.model.conv_0[0].weight.shape[0],args.model.FE1.conv_0[0].weight.shape[0],args.model.conv_1[0].weight.shape[0]]
  
  if args.retrain :
    path = "Models/"
    if os.path.exists(path):
      resultat = sr.load(path + "resultat_" + args.model_name + ".pt")
    else:
      path = os.path.join(os.getcwd(),os.pardir,"Models/")
      if os.path.exists(path):
        resultat = sr.load(path + "resultat_" + args.model_name + ".pt")
      else:
        path = os.getcwd()
        resultat = sr.load(path + "resultat_" + args.model_name + ".pt")
    state_dict = resultat.trained_model.cut_pursuit.state_dict()
    state_dict['Lambda'] = torch.tensor(args.Lambda_init)
    resultat.trained_model.cut_pursuit.load_state_dict(state_dict)
    args.model = resultat.trained_model


  test_set, train_set, validation_set,n_test,n_train,n_valid= insert.creation_set(args, rand_choice = choix_aléatoire)# génération des ensembles de donnée
  # print("set : ",GPUtil.getGPUs()[0].memoryUsed)
  # recalcul des poids des classes dans la loss, et du coeficient au frontière des classes
  train_loader = torch.utils.data.DataLoader(train_set, batch_size=1, shuffle=False, drop_last=False)
  compt_pixel_class = torch.zeros([args.n_class])
  if args.do_augment_front:
    valid_loader = torch.utils.data.DataLoader(validation_set, batch_size=1, shuffle=False, drop_last=False)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=False, drop_last=False)
    compt_pixel_front = torch.zeros(1)
    train_px_front = torch.zeros([n_train,taille_graph[0],taille_graph[1]]).bool()
    validation_px_front = torch.zeros([n_valid,taille_graph[0],taille_graph[1]]).bool()
    test_px_front = torch.zeros([n_test,taille_graph[0],taille_graph[1]]).bool()
    if args.cuda:
      compt_pixel_front = compt_pixel_front.cuda()
  for index, (tiles, gt, connect, gt_complet) in enumerate(train_loader):
    unique,count = torch.unique(gt, sorted=True, return_counts=True)
    if torch.any(unique == 99):
      unique = unique[:-1]
    compt_pixel_class = compt_pixel_class + count[unique]
    if args.do_augment_front:
      px_front = torch.zeros(gt.shape).bool()
      if args.cuda:
        gt = gt.cuda()
        px_front = px_front.cuda()
      for hor in range(-1,1+1):
        for ver in range(-1+abs(hor),1-abs(hor)+1):
          if ver !=0 or hor !=0:
            gt_temp = gt+1
            if hor>0:
              gt_temp = shift.droite(hor,gt_temp)
            else:
              gt_temp = shift.gauche(-hor,gt_temp)
            if ver>0:
              gt_temp = shift.haut(ver,gt_temp)
            else:
              gt_temp = shift.bas(-ver,gt_temp)
            px_front = px_front + (gt == (1-(gt_temp-1)))
      compt_pixel_front = compt_pixel_front + torch.count_nonzero(px_front)
      train_px_front[index] = px_front
  if args.do_augment_front:
    for index, (tiles, gt, connect, gt_complet) in enumerate(valid_loader):
      px_front = torch.zeros(gt.shape).bool()
      if args.cuda:
        gt = gt.cuda()
        px_front = px_front.cuda()
      for hor in range(-1,1+1):
        for ver in range(-1+abs(hor),1-abs(hor)+1):
          if ver !=0 or hor !=0:
            gt_temp = gt+1
            if hor>0:
              gt_temp = shift.droite(hor,gt_temp)
            else:
              gt_temp = shift.gauche(-hor,gt_temp)
            if ver>0:
              gt_temp = shift.haut(ver,gt_temp)
            else:
              gt_temp = shift.bas(-ver,gt_temp)
            px_front = px_front + (gt == (1-(gt_temp-1)))
      validation_px_front[index] = px_front

    for index, (tiles, gt, connect, gt_complet) in enumerate(test_loader):
      px_front = torch.zeros(gt.shape).bool()
      if args.cuda:
        gt = gt.cuda()
        px_front = px_front.cuda()
      for hor in range(-1,1+1):
        for ver in range(-1+abs(hor),1-abs(hor)+1):
          if ver !=0 or hor !=0:
            gt_temp = gt+1
            if hor>0:
              gt_temp = shift.droite(hor,gt_temp)
            else:
              gt_temp = shift.gauche(-hor,gt_temp)
            if ver>0:
              gt_temp = shift.haut(ver,gt_temp)
            else:
              gt_temp = shift.bas(-ver,gt_temp)
            px_front = px_front + (gt == (1-(gt_temp-1)))
      test_px_front[index] = px_front
    if args.cuda:
      train_px_front = train_px_front.cuda()
      validation_px_front = validation_px_front.cuda()
      test_px_front = test_px_front.cuda()
  else:
    train_px_front = None
    validation_px_front = None
    test_px_front = None

  total_px = torch.sum(compt_pixel_class)
  print("total pixel etiqueté : ", total_px)
  weight_class = 1/args.n_class + ((total_px - compt_pixel_class)/(total_px*(args.n_class-1))-1/args.n_class)*coef_weight
  if args.do_augment_front:
    facteur_front = ((total_px-compt_pixel_front)/compt_pixel_front)*coef_front
    args.facteur_front = facteur_front
  if args.cuda:
    args.weight_class = weight_class.cuda()
  else:
    args.weight_class = weight_class
  # print("weight : ",GPUtil.getGPUs()[0].memoryUsed)
  print(args.weight_class)
  mes_temps_entrainement = []

  ### apprentissage et sauvegarde
  print("depart apprentissage")

  if args.edge_detection:
    print("edge_detection")
    mes_temps_entrainement.append(time.time())
    [trained_Lambda,evol_train_Lambda,evol_test_Lambda,evol_valid_Lambda,epoch_save_Lambda] = tf.train_full_Lambda(args, train_set, validation_set, test_set)
    mes_temps_entrainement.append(time.time())
    resultat_Lambda = sc.result_Lambda(trained_Lambda,evol_train_Lambda,evol_test_Lambda,evol_valid_Lambda,epoch_save_Lambda,mes_temps_entrainement[1]-mes_temps_entrainement[0])
    args.model = trained_Lambda
  else :
    resultat_Lambda = None
  if reseau_complet:
    print("reseau_complet")
    mes_temps_entrainement.append(time.time())
    [trained_model,evol_train_TV,evol_test_TV,evol_valid_TV,evol_composante,epoch_valid,epoch_regularisation] = tf.train_full_TV(args, train_set, validation_set, test_set, train_px_front, validation_px_front, test_px_front)
    mes_temps_entrainement.append(time.time())
    resultat = sc.result(trained_model,evol_train_TV,evol_valid_TV,evol_test_TV,evol_composante,mes_temps_entrainement[1]-mes_temps_entrainement[0],epoch_valid,epoch_regularisation)
  else :
    resultat = None

  return resultat_Lambda,resultat

##########################################################################################################################################################
if __name__ == "__main__":
  # définition des paramètre pour le réseau
  name = "test"
  print(name)

  #verifie si un réseau du même nom exist
  path = "Models/"
  if os.path.exists(path):
    filename = path + "resultat_" + name + ".pt"
    assert not os.path.isfile(filename)
  else:
    path = os.path.join(os.getcwd(),os.pardir,"Models/")
    if os.path.exists(path):
      filename = path + "resultat_" + name + ".pt"
      assert not os.path.isfile(filename)
    else:
      path = os.getcwd()
      filename = path + "resultat_" + name + ".pt"
      assert not os.path.isfile(filename)

  ### option de calcul
  Lambda_unique = True
  gradient_analytique = False
  graphe_tronque = False
  diff_fini = False

  pretrain_Lambda = False # récupère un réseau spécifique pour les Lambdas
  name_save_lambda = "edge_detection_leger"
  edge_detection = False # apprend un réseau spécifique pour les Lamdbas
  n_dilation = 4 # nombre de convolution dans le bloque enrichissement 

  reseau_complet = Lambda_unique + gradient_analytique + graphe_tronque + diff_fini
  assert (Lambda_unique + gradient_analytique + graphe_tronque + diff_fini) <= 1

  ### caractéristique réseau
  n_epoch = 100 # nombre maxixmum d'epoch pour l'apprentissage
  batch_size = 16
  n_class = 2
  class_names = ["background","Square"]
  n_channels = 1
  conv_width = [32,32,16]
  niveau_sup = False
  conv_width_sup = [32,32]
  coef_weight = 1 # coefficient reglant l'equilibrage des classes [0,1] ( Si 0 => 1/n_classe; Si 1 => equilibrage en fonction du nombre de pixels)
  cuda = 1
  lr = 1e-3
  coef_drop = 0.5 # coeficient de toute les couche de drop out
  n_epoch_validation = 10 # nombre d'époch avant arret de l'apprentissage si pas d'amelioration ( a partir de la fin de l'apprentissage des lambdas : blocage_epoch + 2*red_grad_lambda)
  set_name = 'square_5_bord' # choix des données (nom du fichier)
  choix_aléatoire = False # choix des images entrainement et validation

  # toute les paire de convolution avec les mêmes paramètres:
  dilation = 2 # seulement la deuxième convolution de chaque paire
  kernel_size = 3 # toute les convolutions

  retrain = False # nouvel entrainement d'un model existant
  model_name = "Lambda_unique_not_reg_batchnorm_update" # nom du model a réentrainé
  blocage_reseau = False # bloque les paramètres du réseau préentrainé (apprentisage de la régularisation seul)

  # scheduler s'applique sur tout les paramètres a partir du début de leur apprentisage (prise en compte de blocage_epoch et de red_grad_lambda)
  scheduler_step = 10 # nb epoch entre chaque changement de lr
  scheduler_gamma = 0.1 # facteur appliqué au lr

  ### caractéristique regularisation
  do_reg = True
  graph_droit = True
  List_Distance = [1,4,8]
  do_use_connect_set = 0
  # 0 : ne pas utilisé les donné sur les arrête
  # 1 : donner les Lambdas "Parfait" (incompatible avec donné satelitaire)
  # 2 : ajouté les donné dans la loss (utilisé avec coef_reg)
  # 3 : donner les Lambdas "Parfait" + générer des résidu (incompatible avec donné satelitaire)
  TV = False # ajoute un terme de variation total dans la loss
  coef_TV = 20

  # génération du graphe lier au donné:
  taille_graph = [256,256] #taille spacial des données
  connectivity = 2

  blocage_epoch = 5 # nombre d'époch sans régularisation (10 trop grand le réseau est trop précis) 
  # (-1 => aprend le modèle sans régularisation puis l'ajoute en utilisant blocage_reseau pour bloqué ou non le premier model)
  reg_sousgrad = False #choix controle Ridge ou Subgrad (si coef_reg = 0 mettre False pour economie de memoire) (pris en compte si do_use_connect_set != 2)
  coef_reg = 0 # poid du controle des lambda dans la loss
  # petit nombre de composante => 10% des arrète coupé sois un coût sur 115000 arrète
  # 94% des pixels deja bien prédis => gain possible estimé 1%
  # coef reg < 1% / 115000 = 9e-8 sois environ 1e-7
  blocage_reg = 0 # nombre d'epoch sans contrôle des lambdas (aprés blocage epoch)
  nb_feature = 1/2 # proportion de feature donnée au lambda ([0,1])
  red_grad_lambda = 0 #nombre d'epoch de reduction des gradiants de lambda (aprés blocage epoch)
  taille_sous_graph_pixel = 1 #taille du sous graph pour le calcul du gradiant (si calcul sur graph tronqué)
  taille_sous_graph_arret = 0 #taille du sous graph pour le calcul du gradiant des lambdas (si calcul sur graph tronqué)
  do_augment_front = False # augmenté la loss au frontière de class (mettre Faux si coef_front = 0 economie de memoire)
  coef_front = 1 # coefficient reglant l'importance des pixels au frontière [0,1]
  logit = False # ajoute les logits pour la generation des Lambdas
  couche_sup_lambda = False # ajout d'une couche suplémentaire sur la génération des lambdas
  Lambda_separe = False # la génération des lambdas a son propre réseau
  conv_width_Lambda = [2,4,2] # utile si Lambda_separe = True
  Lambda_init = 1 # valeur initial de lambda si Lambda unique

  ### caractéristique données
  prop_degrad = 0 # proportion de dégradation de l'etiquetage ([0,1])
  prop_img_train_set = 1 # nombre d'image dans l'ensemble d'entrainement (max * [0,1])
  urban = False # choix entre des donnée urban/not urban or road/not road (mettre a faux si square = True)
  square = True # choix set carré
  # print("parametre : ",GPUtil.getGPUs()[0].memoryUsed)
  ### création des données
  args = sc.argument(Lambda_unique,gradient_analytique,graphe_tronque,diff_fini,pretrain_Lambda,name_save_lambda,edge_detection,n_dilation, \
                    n_epoch,batch_size,n_class,class_names,n_channels,conv_width,niveau_sup,conv_width_sup,cuda,lr,coef_drop,n_epoch_validation,set_name, \
                    dilation,kernel_size,retrain,model_name,blocage_reseau,scheduler_step,scheduler_gamma, \
                    do_reg,graph_droit,List_Distance,do_use_connect_set,TV,coef_TV,taille_graph,connectivity, \
                    blocage_epoch,reg_sousgrad,coef_reg,blocage_reg,nb_feature,red_grad_lambda,taille_sous_graph_pixel, \
                    taille_sous_graph_arret,do_augment_front,logit,couche_sup_lambda,Lambda_separe,conv_width_Lambda,Lambda_init, \
                    prop_degrad,prop_img_train_set,urban,square)
  # print("args : ",GPUtil.getGPUs()[0].memoryUsed)

  resultat_Lambda,resultat = main(args, coef_front, coef_weight, choix_aléatoire, taille_graph, reseau_complet)

  path = "Models/"
  if os.path.exists(path):
    if reseau_complet:
      sr.save(resultat, path + "resultat_" + name + ".pt")
    if args.edge_detection:
      sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + ".pt")
    sr.save(args, path + "args_" + name + ".pt")
  else:
    path = os.path.join(os.getcwd(),os.pardir,"Models/")
    if os.path.exists(path):
      if reseau_complet:
        sr.save(resultat, path + "resultat_" + name + ".pt")
      if args.edge_detection:
        sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + ".pt")
      sr.save(args, path + "args_" + name + ".pt")
    else:
      path = os.getcwd()
      if reseau_complet:
        sr.save(resultat, path + "resultat_" + name + ".pt")
      if args.edge_detection:
        sr.save(resultat_Lambda, path + "resultat_Lambda_" + name + ".pt")
      sr.save(args, path + "args_" + name + ".pt")

  mes_temps_cut_pursuit,mes_temps_backward,mes_temps_reg_value,mes_temps_gene_Lambda,mes_temps_gene_logit,mes_temps_reg = tf.net_def.get_temps()
  
  
  mes_temps_cut_pursuit = torch.tensor(mes_temps_cut_pursuit).float()
  mes_temps_backward = torch.tensor(mes_temps_backward).float()
  mes_temps_reg_value = torch.tensor(mes_temps_reg_value).float()
  mes_temps_gene_Lambda = torch.tensor(mes_temps_gene_Lambda).float()
  mes_temps_gene_logit = torch.tensor(mes_temps_gene_logit).float()
  mes_temps_reg = torch.tensor(mes_temps_reg).float()
  
  print("Mesures Temps : \n")
  print("mean cut pursuit : ",torch.mean(mes_temps_cut_pursuit))
  print("std cut pursuit : ",torch.std(mes_temps_cut_pursuit))
  print("\n")
  print("mean backward : ",torch.mean(mes_temps_backward))
  print("std backward : ",torch.std(mes_temps_backward))
  print("\n")
  print("mean reg_value : ",torch.mean(mes_temps_reg_value))
  print("std reg_value : ",torch.std(mes_temps_reg_value))
  print("\n")
  print("mean gene_Lambda : ",torch.mean(mes_temps_gene_Lambda))
  print("std gene_Lambda : ",torch.std(mes_temps_gene_Lambda))
  print("\n")
  print("mean gene_logit : ",torch.mean(mes_temps_gene_logit))
  print("std gene_logit : ",torch.std(mes_temps_gene_logit))
  print("\n")
  print("mean reg : ",torch.mean(mes_temps_reg))
  print("std reg : ",torch.std(mes_temps_reg))
  print("\n")
  
  mes_temps_pred,mes_temps_backward,mes_temps_optimizer = tf.get_temps()
  
  
  mes_temps_pred = torch.tensor(mes_temps_pred).float()
  mes_temps_backward = torch.tensor(mes_temps_backward).float()
  mes_temps_optimizer = torch.tensor(mes_temps_optimizer).float()
  mes_temps_gene_Lambda = torch.tensor(mes_temps_gene_Lambda).float()
  
  print("Mesures Temps : \n")
  print("mean pred : ",torch.mean(mes_temps_pred))
  print("std pred : ",torch.std(mes_temps_pred))
  print("\n")
  print("mean backward total: ",torch.mean(mes_temps_backward))
  print("std backward total: ",torch.std(mes_temps_backward))
  print("\n")
  print("mean optimizer : ",torch.mean(mes_temps_optimizer))
  print("std optimizer : ",torch.std(mes_temps_optimizer))
  print("\n")
  
  print("\n Fin")