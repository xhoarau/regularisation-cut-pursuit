import math
#format libraries
import h5py
#visualization libraries
import matplotlib.pyplot as plt
import matplotlib.colors as col
#deep learning tools
import torch
import torchvision.transforms as transforms

from PIL import Image
from PIL import ImageOps
from PIL import ImageDraw

import modules.plot_exemple_result as pl_re

import sys,os
import numpy as np

sys.path.append(os.path.join(os.getcwd(),"grid-graph/python/bin"))
sys.path.append(os.path.join(os.getcwd(),os.pardir,"grid-graph/python/bin"))

from grid_graph import grid_to_graph
from grid_graph import edge_list_to_forward_star

def view_Lambda(label, edges, repartition_lambda, std_logit, ax1, ax2, plafond = True):
  index_droite= []
  index_bas = []
  taille = 256
  for i in range(taille*taille):
    if i%taille != taille-1:
      index_droite.append(torch.min(torch.nonzero(edges[:,0]==i)).item())
    else:
      index_droite.append(torch.min(torch.nonzero(edges[:,0]==i-1)).item())
    if i<(taille-1)*taille:
      index_bas.append(torch.max(torch.nonzero(edges[:,0]==i)).item())
    else:
      index_bas.append(torch.max(torch.nonzero(edges[:,0]==i-taille)).item())
  label = (label==1)*2+(label==99)*1

  print("ecart_type logit : ",std_logit)
  print("moyenne lambda : ",torch.mean(repartition_lambda))
  print("ecart_type lambda : ",torch.std(repartition_lambda))

  hue = (label/3)
  sat = repartition_lambda[index_droite]
  print("lambda max h : ", torch.max(sat).item(),", lambda min h : ", torch.min(sat).item())
  if plafond:
    sat = torch.minimum(sat,std_logit*5)
  sat = sat-torch.min(sat)
  sat = (1-((sat/torch.max(sat))*0.8)).cpu()
  val = torch.ones([256,256]).float()
  img = col.hsv_to_rgb(torch.stack((hue,sat.reshape([256,256]),val)).permute(1,2,0))
  ax1.imshow(img)
  ax1.axis('off')

  sat = repartition_lambda[index_bas]
  print("lambda max v : ", torch.max(sat).item(),", lambda min v : ", torch.min(sat).item())
  if plafond:
    sat = torch.minimum(sat,std_logit*5)
  sat = sat-torch.min(sat)
  sat = (1-((sat/torch.max(sat))*0.8)).cpu()
  img = col.hsv_to_rgb(torch.stack((hue,sat.reshape([256,256]),val)).permute(1,2,0))
  ax2.imshow(img)
  ax2.axis('off')

#finds the straight-line distance between two points
def distance(ax, ay, bx, by):
    return math.sqrt((by - ay)**2 + (bx - ax)**2)

#rotates point `A` about point `B` by `angle` radians clockwise.
def rotated_about(ax, ay, bx, by, angle):
    radius = distance(ax,ay,bx,by)
    angle += math.atan2(ay-by, ax-bx)
    return (
        torch.round(bx + radius * math.cos(angle)),
        torch.round(by + radius * math.sin(angle))
    )

set_complet = True # image et verité terrain ou juste image
set_existant = True # utilisé label fait a la main
nom_dossier = "set_carre_5" # nom du dossier si set_complet = False
taille = 256
n_train = 100
n_test = 50
std = 5
name = "square_5_low_label"
img_set = torch.zeros([n_train + n_test,taille,taille])
gt_set = torch.zeros([n_train + n_test,taille,taille])
gt_complet = torch.zeros([n_train + n_test,taille,taille])
transform = transforms.Compose([transforms.PILToTensor()])

if set_complet and not set_existant:
  taille_graph = [taille,taille]
  edges_1, connectivities_1 = grid_to_graph(np.array(taille_graph,dtype='int32'), connectivity = 1, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
  first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(taille_graph),edges_1)# changement de representation
  first_edges = first_edges.astype('uint32')
  adj_vertices = adj_vertices.astype('uint32')
  edges_1[reindex,:] = np.copy(edges_1)
  edges_1 = torch.from_numpy(edges_1.astype("int64")).cuda()
  connectivities_1[reindex] = np.copy(connectivities_1)
  connectivities_1 = torch.sqrt(torch.from_numpy(connectivities_1).cuda())
  gt_lambda_connect1 = torch.zeros([n_train + n_test,connectivities_1.shape[0]])

  edges_2, connectivities_2 = grid_to_graph(np.array(taille_graph,dtype='int32'), connectivity = 2, compute_connectivities = True, graph_as_forward_star = False, row_major_index = True)# calcul du graph des adjacance
  first_edges,adj_vertices, reindex = edge_list_to_forward_star(np.prod(taille_graph),edges_2)# changement de representation
  first_edges = first_edges.astype('uint32')
  adj_vertices = adj_vertices.astype('uint32')
  edges_2[reindex,:] = np.copy(edges_2)
  edges_2 = torch.from_numpy(edges_2.astype("int64")).cuda()
  connectivities_2[reindex] = np.copy(connectivities_2)
  connectivities_2 = torch.sqrt(torch.from_numpy(connectivities_2).cuda())
  gt_lambda_connect2 = torch.zeros([n_train + n_test,connectivities_2.shape[0]])

if set_existant:
  for indice_img in range(img_set.shape[0]):
    img_set[indice_img] = transform(Image.open(nom_dossier + "/" + str(indice_img) + "_square_" + str(std) + ".png"))/255+torch.randn([taille,taille])*std
    gt_set[indice_img] = transform(Image.open(nom_dossier + "/" + str(indice_img) + "_square_" + str(std) + "_label.png"))
    gt_set[indice_img] = ((gt_set[indice_img]==0) * 99) + ((gt_set[indice_img]==2) * 1)
    gt_complet[indice_img] = transform(Image.open(nom_dossier + "/" + str(indice_img) + "_square_" + str(std) + ".png"))/255
else:
  for indice_img in range(img_set.shape[0]):
    ncarre = torch.randint(3,6,[1])
    image = Image.new('1', (taille, taille), 0)
    draw = ImageDraw.Draw(image)
    for n in range(ncarre):
      square_center = (torch.randint(0,taille,[1]),torch.randint(0,taille,[1]))
      square_length = torch.randint(int(taille/10),int(taille/4),[1])
      square_vertices = (
          (square_center[0] + square_length / 2, square_center[1] + square_length / 2),
          (square_center[0] + square_length / 2, square_center[1] - square_length / 2),
          (square_center[0] - square_length / 2, square_center[1] - square_length / 2),
          (square_center[0] - square_length / 2, square_center[1] + square_length / 2)
      )
      angle = torch.randint(0,90,[1])
      square_vertices = [rotated_about(x,y, square_center[0], square_center[1], math.radians(angle)) for x,y in square_vertices]
      draw.polygon(square_vertices, fill=1)
    if set_complet:
      img_set[indice_img] = transform(image)/255+torch.randn([taille,taille])*std
      gt_set[indice_img] = transform(image)/255
      gt_lambda_connect1[indice_img] = (gt_set.reshape([gt_set.shape[0],gt_set.shape[1]*gt_set.shape[2]])[indice_img,edges_1[:,0]] == gt_set.reshape([gt_set.shape[0],gt_set.shape[1]*gt_set.shape[2]])[indice_img,edges_1[:,1]])*1
      gt_lambda_connect2[indice_img] = (gt_set.reshape([gt_set.shape[0],gt_set.shape[1]*gt_set.shape[2]])[indice_img,edges_2[:,0]] == gt_set.reshape([gt_set.shape[0],gt_set.shape[1]*gt_set.shape[2]])[indice_img,edges_2[:,1]])*1
    else:
      image.save(nom_dossier + "/" + str(indice_img) + "_square_" + str(std) + ".png","png")

if set_complet or set_existant:
  fig = plt.figure(figsize=(10, 5)) #adapted dimension
  subplot_index = 1 #keep track of current subplot
  ax = fig.add_subplot(1, 2, subplot_index, aspect='equal')
  subplot_index += 1
  plt.imshow(img_set[img_set.shape[0]-1],cmap='gray')
  ax = fig.add_subplot(1, 2, subplot_index, aspect='equal')
  subplot_index += 1
  plt.imshow(gt_set[gt_set.shape[0]-1],cmap='gray')
  plt.savefig(name + "_exemple.png")
  plt.close()

  hf = h5py.File(name + '.hdf5', 'w')
  hf.create_dataset('train_observation',data = img_set[:n_train])
  hf.create_dataset('train_gt',data = gt_set[:n_train])
  hf.create_dataset('test_observation',data = img_set[n_train:])
  hf.create_dataset('test_gt',data = gt_set[n_train:])
  if set_existant:
    hf.create_dataset('train_gt_complet',data = gt_complet[:n_train])
    hf.create_dataset('test_gt_complet',data = gt_complet[n_train:])
  else:
    hf.create_dataset('train_connect1_gt',data = gt_lambda_connect1[:n_train])
    hf.create_dataset('test_connect1_gt',data = gt_lambda_connect1[n_train:])
    hf.create_dataset('train_connect2_gt',data = gt_lambda_connect2[:n_train])
    hf.create_dataset('test_connect2_gt',data = gt_lambda_connect2[n_train:])

  hf.close()

if set_complet and not set_existant:
  fig = plt.figure(figsize=(10, 5),dpi=750) #adapted dimension
  subplot_index = 1
  ax1 = fig.add_subplot(1, 2, subplot_index, aspect='equal')
  if subplot_index <= 2 : 
    ax1.set(title='Lambda_h')
  subplot_index += 1
  ax2 = fig.add_subplot(1, 2, subplot_index)
  if subplot_index <= 2 : 
    ax2.set(title='Lambda_v')
  subplot_index += 1
  view_Lambda(gt_set[gt_set.shape[0]-1], edges_1,gt_lambda_connect1[gt_set.shape[0]-1] , 1, ax1 = ax1, ax2 = ax2,plafond = False)

  plt.savefig(name + "_bord.png")
  plt.close()