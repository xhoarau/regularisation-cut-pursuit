import matplotlib.pyplot as plt
import matplotlib.colors as col
import numpy as np
import torch
import modules.insertion_donne as insert
import modules.network_definition as net_def
import modules.storage_class as sc

#############################################################
#functions used for visualization
#############################################################

def view_rgb(tile, args, ax = None):
  """ show the rgb values of the tile in figure ax"""
  if args.square:
    if ax==None:
      fig = plt.figure(figsize=(10, 10))
      ax = fig.add_subplot(1, 1, 1, aspect='equal')
    tile_corrected = np.minimum(np.maximum(tile[:3,:,], 0), 1) #normalization
    plt.imshow(np.squeeze(tile),cmap='gray') #put channels back as dim 3
  else:
    if ax==None:
      fig = plt.figure(figsize=(10, 10))
      ax = fig.add_subplot(1, 1, 1, aspect='equal')
    tile_corrected = np.minimum(np.maximum(tile[:3,:,], 0), 1) #normalization
    plt.imshow(tile_corrected.transpose(0,2).transpose(0,1)) #put channels back as dim 3
  plt.axis('off')
  
def view_infrared(tile, ax = None):
  """ show the infrared tile in figure ax"""
  if ax==None:
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1, aspect='equal')
  tile_corrected = np.minimum(np.maximum(tile[3,:,:], 0), 1) #normalization
  plt.imshow(tile_corrected,cmap='hot')
  plt.axis('off')

def view_labels(label, ax = None, mask = None,):
  """ show the ground truth with a color code corresponding to labels in figure ax
  if mask is not None, then set the nonzero vales of masks to the unseen label
  """
  if ax==None:
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1, aspect='equal')
  if mask is not None:
    label.reshape([256*256])[mask] = 99
  n_pixel = label.shape[1]
  colors = np.zeros((n_pixel,n_pixel,3))
  colors[np.where(label==99)] = [1  ,1  ,1  ] #not labelled
  colors[np.where(label==0)]  = [1  ,0.8,0.8] #building limit
  colors[np.where(label==1)]  = [0  ,0  ,1  ] #water
  colors[np.where(label==2)]  = [0.9,0.9,0  ] #fields
  colors[np.where(label==3)]  = [0.5,0.5,0.5] #road
  colors[np.where(label==4)]  = [0  ,.8  ,0 ] #vegetation
  colors[np.where(label==5)]  = [1,  0  ,0  ] #building
  plt.imshow(colors)
  plt.axis('off')
  
def view_error(pred, gt, ax = None):
  """ show the error between pred and gt with colorcode:
 green when 'gt'='pred', red when 'gt'!='pred' and black
 when unannotated (gt = 99). Display in figure ax"""
  if ax==None:
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1, aspect='equal')
  n_pixel = gt.shape[1]
  colors = np.zeros((n_pixel,n_pixel,3))
  colors[np.where(pred==gt.squeeze())] = [0, 1, 0] #correct prediction
  colors[np.where(pred!=gt.squeeze())] = [1, 0, 0] #error
  colors[np.where(gt.squeeze()==99)] = [0, 0, 0]   #unannotated 
  plt.imshow(colors)
  plt.axis('off')

def view_Lambda(label, args, repartition_lambda, std_logit, ax1, ax2, plafond = True):
  index_droite= []
  index_bas = []
  taille = 256
  for i in torch.unique(args.edges):
    indices = torch.nonzero(args.edges[:,0]==i)
    if indices.shape[0] != 0:
      index_droite.append(indices[0])
      if i%taille != taille-1 and i<(taille-1)*taille:
        index_bas.append(indices[1])
      else:
        index_bas.append(indices[0])
    else:
      index_droite.append(torch.nonzero(args.edges[:,0]==i-1)[0])
      index_bas.append(torch.nonzero(args.edges[:,0]==i-1)[0])
  label = (label==1)*2+(label==99)*1
  
  print("ecart_type logit : ",std_logit[len(repartition_lambda)-1])
  print("moyenne lambda : ",torch.mean(repartition_lambda[len(repartition_lambda)-1]))
  print("ecart_type lambda : ",torch.std(repartition_lambda[len(repartition_lambda)-1]))

  hue = (label/3)
  sat = repartition_lambda[len(repartition_lambda)-1][0,index_droite]
  print("lambda max h : ", torch.max(sat).item(),", lambda min h : ", torch.min(sat).item())
  if plafond:
    sat = torch.minimum(sat,std_logit[len(repartition_lambda)-1]*5)
  sat = sat-torch.min(sat)
  sat = (1-((sat/torch.max(sat))*0.8)).cpu()
  val = torch.ones([256,256]).float()
  img = col.hsv_to_rgb(torch.stack((hue,sat.reshape([256,256]),val)).permute(1,2,0))
  ax1.imshow(img)
  ax1.axis('off')

  sat = repartition_lambda[len(repartition_lambda)-1][0,index_bas]
  print("lambda max v : ", torch.max(sat).item(),", lambda min v : ", torch.min(sat).item())
  if plafond:
    sat = torch.minimum(sat,std_logit[len(repartition_lambda)-1]*5)
  sat = sat-torch.min(sat)
  sat = (1-((sat/torch.max(sat))*0.8)).cpu()
  img = col.hsv_to_rgb(torch.stack((hue,sat.reshape([256,256]),val)).permute(1,2,0))
  ax2.imshow(img)
  ax2.axis('off')

def view_hist(args, repartition_lambda, std_logit, ax):
  ax.hist(repartition_lambda[len(repartition_lambda)-1].detach().cpu(),bins = max(10,int(torch.max(repartition_lambda[len(repartition_lambda)-1]).item()/10)))
  ax.axvline(x=std_logit[len(repartition_lambda)-1].cpu()*5, ymin=0, ymax=torch.max(repartition_lambda[len(repartition_lambda)-1]).cpu(), color='k', linewidth=2)

def view_comp(label, args, repartition_Comp, ax):
  label = (label==1)*2+(label==99)*1
  hue = (label/3)
  val = repartition_Comp[len(repartition_Comp)-1][0]
  print("nb Comp : ", torch.max(val).item())
  val = val-torch.min(val)
  val = (1-((val/torch.max(val))*0.8)).cpu()
  sat = torch.ones([256,256]).float()
  img = col.hsv_to_rgb(torch.stack((hue,sat,val)).permute(1,2,0))
  ax.imshow(img)
  ax.axis('off')

def viewer(args, model, epoch_save, name, n_shown = 3, category = 'cig', train = True, L_pred=True):
  """ plot n_shown random tiles train/test set with the following visuals:
  if 'c' in category : rgb color
  if 'i' in category : infrared
  if 'g' in category : ground truth
  if 'p' in category : prediction
  if 'e' in category : error
  if 'l' in category : lambda
  Note that for 'p' or 'e' you need to add a trained model as input
  If specified, the masks will set the given pixels' prediction to 99
  """
  n_category = len(category) #number of types of image to show
  if 'l' in category: 
    n_category = n_category +1
  if 'L' in category: 
    n_category = n_category +1
  fig = plt.figure(figsize=(n_category * 5, n_shown * 5),dpi=750) #adapted dimension
  
  subplot_index = 1 #keep track of current subplot

  test_set, train_set, validation_set = insert.creation_set(args)
  
  #chose random tiles
  tile_indices = np.random.choice(len(train_set), n_shown) if train \
  else np.random.choice(len(test_set), n_shown)

  # if train:
  #   assert n_train > n_shown
  # else:
  #   assert n_test > n_shown
  # tile_indices = np.arange(0, n_shown, 1)
  
  for tile_index in tile_indices:
    
    if train:
      tile, gt = train_set.__getitem__(tile_index)
    else:
      tile, gt = test_set.__getitem__(tile_index)
    pred = model(args, tile[None,:,:,:].cuda(), epoch_save)[0].cpu().argmax(1).squeeze()
    
    if 'c' in category:
      ax = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
      if subplot_index <= n_category : 
        ax.set(title='RGB')
      view_rgb(tile, args, ax = ax)
      subplot_index += 1
    if not args.square:
      if 'i' in category:  
        ax = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
        if subplot_index <= n_category : 
          ax.set(title='Infrared')
        view_infrared(tile, ax = ax)
        subplot_index += 1
    if 'g' in category:  
      ax = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
      if subplot_index <= n_category : 
        ax.set(title='Ground Truth')
      view_labels(gt, ax = ax)
      subplot_index += 1
    if 'p' in category:  
      ax = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
      if subplot_index <= n_category : 
        ax.set(title='Prediction')
      view_labels(pred, ax = ax,mask = np.nonzero(gt.reshape([256*256]) == 99))
      subplot_index += 1  
    if 'e' in category: 
      ax = fig.add_subplot(n_shown, n_category, subplot_index)
      if subplot_index <= n_category : 
        ax.set(title='Error')
      view_error(pred, gt, ax = ax)
      subplot_index += 1
    if args.do_reg:
      if 'l' in category:
        repartition_lambda,std_logit = net_def.get_repart()
        ax1 = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
        if subplot_index <= n_category : 
          ax1.set(title='Lambda_h')
        subplot_index += 1
        ax2 = fig.add_subplot(n_shown, n_category, subplot_index)
        if subplot_index <= n_category : 
          ax2.set(title='Lambda_v')
        subplot_index += 1
        if L_pred:
          view_Lambda(pred, args, repartition_lambda, std_logit, ax1 = ax1, ax2 = ax2)
        else:
          view_Lambda(gt, args, repartition_lambda, std_logit, ax1 = ax1, ax2 = ax2)
      if 'C' in category:
        repartition_Comp = net_def.get_repart_comp()
        ax = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
        if subplot_index <= n_category : 
          ax.set(title='Composantes')
        subplot_index += 1
        if L_pred:
          view_comp(pred, args, repartition_Comp, ax = ax)
        else:
          view_comp(gt, args, repartition_Comp, ax = ax)
      if 'L' in category:
        repartition_lambda,std_logit = net_def.get_repart()
        ax1 = fig.add_subplot(n_shown, n_category, subplot_index, aspect='equal')
        if subplot_index <= n_category : 
          ax1.set(title='Lambda_H')
        subplot_index += 1
        ax2 = fig.add_subplot(n_shown, n_category, subplot_index)
        if subplot_index <= n_category : 
          ax2.set(title='Lambda_V')
        subplot_index += 1
        if L_pred:
          view_Lambda(pred, args, repartition_lambda, std_logit, ax1 = ax1, ax2 = ax2, plafond = False)
        else:
          view_Lambda(gt, args, repartition_lambda, std_logit, ax1 = ax1, ax2 = ax2, plafond = False)
      if 'h' in category:
        ax = fig.add_subplot(n_shown, n_category, subplot_index)
        if subplot_index <= n_category : 
          ax.set(title='hist')
        view_hist(args,repartition_lambda, std_logit, ax)
        subplot_index += 1

  plt.savefig(name + ".png")
  plt.close()