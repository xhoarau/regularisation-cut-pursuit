import modules.storage_class as sc
import modules.save_result as sr
import modules.insertion_donne as insert
import modules.train_fonctions as tf
import modules.Confusion_Matrix as CM
import modules.network_definition as net_def
import modules.plot_evol as pl_ev
import modules.plot_exemple_result as pl_re
import os

import numpy as np
import torch

name = "reseau_lambda_separe_b5"
print(name)
evol = False # plot l'evolution au cours de l'apprentissage
evaluation = False # evalure le modèle sur l'ensemble d'entrainement

img = True # generer une image avec des exemples de resultats
train = False # choix de l'ensemble
n_shown = 3 #nombre d'image a traité
category = "cgplLhC" 
  # if 'c' in category : rgb color
  # if 'i' in category : infrared
  # if 'g' in category : ground truth
  # if 'p' in category : prediction
  # if 'e' in category : error
  # if 'l' in category : lambda avec plafond (5*sigma_logit)
  # if 'L' in category : lambda sans plafond
  # if 'h' in category : histogram lambda
  # if 'C' in category : Composante
L_pred = False # choix d'affichage des Lambda gt ou pred


path = "Models/"
if os.path.exists(path):
  resultat = sr.load(path + "resultat_" + name + ".pt")
  args = sr.load(path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")

print("durée entrainement(s) = ",resultat.duree_entrainement)
if hasattr(resultat,"epoch_regularisation"):
  args.epoch_regularisation = resultat.epoch_regularisation

if hasattr(resultat,"epoch_regularisation"):
  print("epoch regularisation = ",resultat.epoch_regularisation)

if evaluation:
  test_set = insert.creation_set(args)[0]
  cm, loss, loss_reg, loss_total = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set)
  print('Test %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f Loss reg: %1.4f Loss_total: %1.4f' % \
          (resultat.epoch_save, cm.overall_accuracy()*100, cm.class_IoU(), loss, loss_reg, loss_total))
  List_composante = net_def.get_composante()
  print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante).float()))
  print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante).float()))


if evol:
  pl_ev.plot_evol_IoU(resultat.evol_train_TV, "Train_" + name, resultat.epoch_save)
  pl_ev.plot_evol_IoU(resultat.evol_valid_TV, "Valid_" + name, resultat.epoch_save)
  pl_ev.plot_evol_IoU(resultat.evol_test_TV, "test_" + name, resultat.epoch_save)
  if args.do_reg:
    pl_ev.plot_evol_coposante(resultat.evol_composante, args, name, resultat.epoch_save)

if img:
  pl_re.viewer(args, resultat.trained_model, resultat.epoch_save, name = name, n_shown = n_shown, category = category, train = train, L_pred=L_pred)

