import numpy as np
import torch
import time
import copy
import sys,os

import modules.insertion_donne as insert
import modules.save_result as sr
import modules.train_fonctions as tf
import modules.storage_class as sc

# définition des paramètre pour le réseau
name = "Test"

path = "Models/"
if os.path.exists(path):
  filename = path + "resultat_" + name + ".pt"
  assert not os.path.isfile(filename)
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    filename = path + "resultat_" + name + ".pt"
    assert not os.path.isfile(filename)
  else:
    path = os.getcwd()
    filename = path + "resultat_" + name + ".pt"
    assert not os.path.isfile(filename)

# caractéristique réseau
n_epoch = 100 # nombre maxixmum d'epoch pour l'apprentissage
batch_size = 16
n_class = 2
class_names = ["back_ground","Square"]
n_channels = 1
conv_width = [32,32,16]
coef_weight = 1 # coefficient reglant l'equilibrage des classes [0,1] ( Si 0 => 1/n_classe; Si 1 => equilibrage en fonction du nombre de pixels)
cuda = 1
lr = 5e-3
coef_drop = 0.5
n_epoch_validation = 10 # nombre d'époch avant arret de l'apprentissage si pas damelioration ( a partir de la fin de l'apprentissage des lambdas : blocage_epoch + 2*red_grad_lambda)
set_name = 'square_5' # choix des données (nom du fichier)
dilation = 1
kernel_size = 3
Lambda_init = 1
retrain = True
model_name = "Lambda_unique_not_reg_batchnorm_update"
blocage_reseau = True

# caractéristique regularisation
do_reg = True
taille_graph = [256,256] #taille spacial des données
blocage_epoch = 0 # nombre d'époch sans régularisation
blocage_reg = 0 # nombre d'epoch sans contrôle des lambdas (aprés blocage epoch)
red_grad_lambda = 10 #nombre d'epoch de reduction des gradiants de lambda (aprés blocage epoch)

# caractéristique données
prop_degrad = 0 # proportion de dégradation de l'etiquetage ([0,1])
prop_img_train_set = 1 # nombre d'imga dans l'ensemble d'entrainement (160 * [0,1])
urban = False # choix entre des donnée urban/not urban or road/not road
square = True # choix set carré

args = sc.argument(n_epoch,batch_size,n_class,class_names,n_channels,conv_width,cuda,lr,coef_drop, \
                  n_epoch_validation,set_name,dilation,kernel_size,Lambda_init,retrain,model_name,blocage_reseau,taille_graph, \
                  do_reg,blocage_epoch,blocage_reg,red_grad_lambda, \
                  prop_degrad,prop_img_train_set,urban,square)

test_set, train_set, validation_set = insert.creation_set(args)# génération des ensembles de donnée

# recalcul des poids des classes dans la loss
loader = torch.utils.data.DataLoader(train_set, batch_size=1, shuffle=False, drop_last=False)
compt_pixel_class = torch.zeros([n_class])
for index, (tiles, gt) in enumerate(loader):
  unique,count = torch.unique(gt, sorted=True, return_counts=True)
  if torch.any(unique == 99):
    unique = unique[:-1]
  compt_pixel_class = compt_pixel_class + count[unique]
total_px = torch.sum(compt_pixel_class)
weight_class = 1/n_class + ((total_px - compt_pixel_class)/(total_px*(n_class-1))-1/n_class)*coef_weight
if cuda:
  args.weight_class = weight_class.cuda()
else:
  args.weight_class = weight_class

mes_temps_entrainement = []

print("depart apprentissage")
mes_temps_entrainement.append(time.time())
[trained_model,evol_train_TV,evol_test_TV,evol_valid_TV,evol_composante,epoch_valid] = tf.train_full_TV(args, train_set, validation_set, test_set)
mes_temps_entrainement.append(time.time())

resultat = sc.result(trained_model,evol_train_TV,evol_valid_TV,evol_test_TV,evol_composante,mes_temps_entrainement[1]-mes_temps_entrainement[0],epoch_valid)

path = "Models/"
if os.path.exists(path):
  sr.save(resultat, path + "resultat_" + name + ".pt")
  sr.save(args, path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    sr.save(resultat, path + "resultat_" + name + ".pt")
    sr.save(args, path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    sr.save(resultat, path + "resultat_" + name + ".pt")
    sr.save(args, path + "args_" + name + ".pt")

print("Fin")