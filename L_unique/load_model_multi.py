import modules.storage_class as sc
import modules.save_result as sr
import modules.insertion_donne as insert
import modules.train_fonctions as tf
import modules.Confusion_Matrix as CM
import modules.network_definition as net_def
import modules.plot_evol as pl_ev
import modules.plot_exemple_result as pl_re
import os

import numpy as np
import torch

name = "reseau_lambda_multi_s1e7"

img_train = True
img_valid = True
img_test = True
img_comp = True
img_exemple = True
train = False # choix de l'ensemble
n_shown = 3 #nombre d'image a traité
category = "cgplLh" 
  # if 'c' in category : rgb color
  # if 'i' in category : infrared
  # if 'g' in category : ground truth
  # if 'p' in category : prediction
  # if 'e' in category : error
  # if 'l' in category : lambda avec plafond (5*sigma_logit)
  # if 'L' in category : lambda sans plafond
  # if 'h' in category : histogram lambda
L_pred = False # choix d'affichage des Lambda gt ou pred


print(name)

path = "Models/"
if os.path.exists(path):
  resultat = sr.load(path + "resultat_" + name + ".pt")
  args = sr.load(path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")

weight_class = torch.stack(resultat.List_weight_class)
print("mean weight class : ",torch.mean(weight_class,dim = 0))
print("std weight class : ",torch.std(weight_class,dim = 0))

print("mean epoch save : ",torch.mean(torch.tensor(resultat.List_epoch_valid).float(),dim = 0))
print("std epoch save : ",torch.std(torch.tensor(resultat.List_epoch_valid).float(),dim = 0))

print("mean temps : ",torch.mean(torch.tensor(resultat.List_temps_entrainement).float(),dim = 0))
print("std temps : ",torch.std(torch.tensor(resultat.List_temps_entrainement).float(),dim = 0))

nb_composante_fin = []
for indice in range(len(resultat.List_evol_composante)):
  num = "_" + str(indice)
  evol = resultat.List_evol_composante[indice]
  if img_comp:
    pl_ev.plot_evol_coposante(evol, args, name + num, resultat.List_epoch_valid[indice])
  indice_dernier_test = int(np.max(np.multiply(np.multiply(evol[:,0]!=0,evol[:,3]!=0),evol[:,0])))
  nb_composante_fin.append(evol[indice_dernier_test,3])

print("mean composante fin : ",torch.mean(torch.tensor(nb_composante_fin).float(),dim = 0))
print("std composante fin : ",torch.std(torch.tensor(nb_composante_fin).float(),dim = 0))

epoch_fin = []
for indice in range(len(resultat.List_evol_train_TV)):
  num = "_" + str(indice)
  evol = resultat.List_evol_train_TV[indice]
  if img_train:
    pl_ev.plot_evol_IoU(evol, "Train_" + name + num, resultat.List_epoch_valid[indice])
  epoch_fin.append(int(max(evol[:,0])+1))

print("mean epoch fin : ",torch.mean(torch.tensor(epoch_fin).float(),dim = 0))
print("std epoch fin : ",torch.std(torch.tensor(epoch_fin).float(),dim = 0))

if img_valid:
  for indice in range(len(resultat.List_evol_valid_TV)):
    num = "_" + str(indice)
    pl_ev.plot_evol_IoU(resultat.List_evol_valid_TV[indice], "Valid_" + name + num, resultat.List_epoch_valid[indice])

IoU = []
for indice in range(len(resultat.List_evol_test_TV)):
  if img_test:
    num = "_" + str(indice)
    pl_ev.plot_evol_IoU(resultat.List_evol_test_TV[indice], "Test_" + name + num, resultat.List_epoch_valid[indice])
  IoU.append(resultat.List_evol_test_TV[indice][int(max(resultat.List_evol_test_TV[indice][:,0])),2])

print("mean IoU : ",torch.mean(torch.tensor(IoU).float(),dim = 0))
print("std IoU : ",torch.std(torch.tensor(IoU).float(),dim = 0))

if img_exemple:
  for index_model in range(len(resultat.List_trained_model)):
    pl_re.viewer(args, resultat.List_trained_model[index_model], resultat.List_epoch_valid[index_model], n_shown = n_shown, category = category, train = train, L_pred=L_pred, name = name + "_" + str(index_model))