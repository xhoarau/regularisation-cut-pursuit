import numpy as np
import torch
import torch.nn as nn
from torch.autograd.function import once_differentiable
from torch_scatter import scatter_mean
from torch.autograd import Function
import copy
import sys,os

sys.path.append(os.path.join(os.getcwd(),"parallel-cut-pursuit/python/wrappers"))
sys.path.append(os.path.join(os.getcwd(),os.pardir,"parallel-cut-pursuit/python/wrappers"))
from cp_prox_tv import cp_prox_tv

import multiprocessing as mp
import threading as th

List_composante = []
repartition_Comp = []

def reset_repart():
  List_composante[:] = []
  repartition_Comp[:] = []

def get_repart_comp():
  return repartition_Comp

def get_composante():
  return List_composante

###################################################
#Création du réseau
###################################################

# couche custom

def use_cp_prox_tv(args,num,img,Lambda,output,List_Comp, connectivities, nb_proc=1):#calcule cp_prox_tv sur l'image et l'enregistre a l'indice donné
  Comp,rX = cp_prox_tv(img, args.first_edges, args.adj_vertices, edge_weights =  (Lambda/connectivities).cpu().detach().numpy(),verbose=0, max_num_threads=nb_proc) #.cpu.detach.numpy pour récupérer une valeur utilisable
  output[num,0,:,:]=torch.from_numpy(rX[Comp].reshape([256,256]))#reviens a la forme d'origine et reviens en torch (transfert vers cuda)
  List_Comp[num]=Comp.astype("int64")# conserve Comp pour backward

class cut_pursuit(Function):
  @staticmethod
  def forward(ctx, args, input1, Lambda, connectivities, epoch):
    # Initialisation
    batch_img = (input1[:,0,:,:]-input1[:,1,:,:]) #calcule de la diférence entre les deux classes
    output = torch.zeros(input1.shape)
    List_Comp=[np.arange(input1.shape[2]*input1.shape[3])]*input1.shape[0]
    list_process = []

    # image a régularisé
    # reg = ((Lambda[:,int((8/10)*Lambda.shape[1])] > ((1e-3)*torch.std(batch_img,unbiased=False,dim=[1,2]))) * (epoch >= args.blocage_epoch))
    # index_reg = torch.arange(0,batch_img.shape[0],1)[reg]
    # index_not_reg = torch.arange(0,batch_img.shape[0],1)[torch.logical_not(reg)]

    # Calcul
    assert mp.cpu_count() > input1.shape[0] # le batch ne dois pas dépassé le nombre de coeur du processeur pour la paralélisation
    if torch.max(Lambda>(1e-3))*torch.std(batch_img,unbiased=False) and epoch>=args.blocage_epoch:
      reg = True
      for index_batch in range(batch_img.shape[0]):
        list_process.append(th.Thread(target=use_cp_prox_tv,args=(args,index_batch,batch_img[index_batch,:,:].cpu().detach().numpy().transpose(),Lambda,output,List_Comp,connectivities))) #math.floor(mp.cpu_count()/input1.shape[0])
        list_process[len(list_process)-1].start()
      for p in list_process:
        p.join()

      List_Comp = torch.tensor(np.array(List_Comp),device = "cuda",dtype=torch.int64)
    
      args.compte_composante.append(torch.mean(torch.max(List_Comp,dim = 1)[0].float()))
      if input1.shape[0] == 1:
        List_composante.append(torch.max(List_Comp,dim = 1)[0])
        repartition_Comp.append(List_Comp.detach().reshape([input1.shape[0],input1.shape[2],input1.shape[3]]))
    
      output = output.cuda()
    else :
      reg = False
      output = output.cuda()
      output[:,0,:,:] = batch_img# pas de régularisation => input = output

    ctx.component_List_Comp = List_Comp
    ctx.edges = args.edges
    ctx.epoch = epoch # numéro d'epoch pour l'importance de l'aprentisage de lambda
    ctx.blocage_epoch = args.blocage_epoch
    ctx.red_grad_lambda = args.red_grad_lambda
    ctx.output = copy.deepcopy(output[:,0,:,:]) # séparation de output et ctx.output pour evité la perte memoire
    ctx.reg = reg

    return output
  
  @staticmethod
  @once_differentiable
  def backward(ctx, grad_output):
  
    #récuperation
    List_Comp = ctx.component_List_Comp
    edges = ctx.edges
    epoch = ctx.epoch
    blocage_epoch = ctx.blocage_epoch
    red_grad_lambda = ctx.red_grad_lambda
    output = ctx.output.detach()
    reg = ctx.reg
    
    output = output.reshape([output.shape[0],output.shape[1]*output.shape[2]])

    grad_input = torch.zeros(grad_output.shape,device='cuda')
    grad_output = grad_output[:,0,:,:]
    grad_output_squeeze = grad_output.reshape([grad_output.shape[0],grad_output.shape[1]*grad_output.shape[2]])
    grad_lambda = torch.zeros([grad_output.shape[0],edges.shape[0]],device = 'cuda')
    
    if reg:
      # # calcul grad input reg
      # calcule de la somme des gradient de chaque composente et le divise par la taille de la composante
      Comp_val = torch.zeros([grad_output.shape[0],torch.max(List_Comp).int().item()+1],device = 'cuda')
      Comp_val = scatter_mean(grad_output_squeeze, List_Comp, dim=1)
      # redistribue les valeurs des composente sour la forme de l'image
      grad_input[:,0,:,:] = torch.gather(Comp_val,1,List_Comp).reshape([grad_output.shape[0],grad_output.shape[1],grad_output.shape[2]])
      grad_input[:,1,:,:] = -grad_input[:,0,:,:]
      
      # # calcul grad_lambda reg
      List_taille = torch.zeros([List_Comp.shape[0],torch.max(List_Comp)+1],device  = 'cuda')
      for i in range(List_Comp.shape[0]):
        List_taille[i,:torch.max(List_Comp[i])+1] = torch.unique(List_Comp[i],return_counts = True)[1]

      grad_lambda = (torch.sign(output[:,edges[:,0]]-output[:,edges[:,1]])/torch.gather(List_taille,1,List_Comp[:,edges[:,1]])) * grad_output_squeeze[:,edges[:,1]]
      grad_lambda = grad_lambda - (torch.sign(output[:,edges[:,0]]-output[:,edges[:,1]])/torch.gather(List_taille,1,List_Comp[:,edges[:,0]])) * grad_output_squeeze[:,edges[:,0]]
    else:
      # # calcul grad input not reg
      grad_input[:,0,:,:] = grad_output # de même pas de regularisation => gradin = gradout
      grad_input[:,1,:,:] = -grad_input[:,0,:,:]
      
      # # calcul grad_lambda not reg
      grad_lambda = (torch.sign(output[:,edges[:,0]]-output[:,edges[:,1]]))*grad_output_squeeze[:,edges[:,1]]
      grad_lambda = grad_lambda - (torch.sign(output[:,edges[:,0]]-output[:,edges[:,1]]))*grad_output_squeeze[:,edges[:,0]]
      
    # blokage de l'aprentissage de lambda sur les premières époch
    if epoch < blocage_epoch:
      grad_lambda = grad_lambda*0
    else:
      if  epoch < blocage_epoch + red_grad_lambda:
        grad_lambda = grad_lambda * ((epoch - blocage_epoch + 1)/red_grad_lambda)

    grad_lambda = torch.sum(grad_lambda)

    return None,grad_input,grad_lambda,None,None

class Custom_layer(nn.Module):
  def __init__(self,lambda_init):
    super().__init__()
    self.Lambda = nn.Parameter(torch.tensor(lambda_init).float(),requires_grad=True)
  
  def forward(self,args,input1,connectivities,epoch):
    return cut_pursuit.apply(args, input1, self.Lambda, connectivities, epoch)
  
  def get_lambda(self):
    return self.Lambda
    

# réseau

class SegNet(nn.Module):
  """
  SegNet network for semantic segmentation
  """
  
  def __init__(self, args):
    """
    initialization function
    """
    super(SegNet, self).__init__() #necessary for all classes extending the module class

    self.is_cuda = args.cuda
    
    #checks that the conv widths are compatible, throws an error otherwise
    assert(args.conv_width[0] == args.conv_width[1])

    if hasattr(args,"dilation"):
      padding = int(((args.kernel_size+2*args.dilation-2)-1)/2)
      kernel_size = args.kernel_size
      dilation = args.dilation
    else:
      padding = 1
      kernel_size = 3
      dilation = 1
    
    self.maxpool = nn.MaxPool2d(2,2,return_indices=True) #maxpooling layer
    self.unpool = nn.MaxUnpool2d(2,2) #unpooling layer
    #encoder (remember that conv_{i0} and conv_{i1} have the same width d_i = conv_width[i]
    self.conv00 = nn.Sequential(nn.Conv2d(in_channels=args.n_channels, out_channels=args.conv_width[0] , kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[0],track_running_stats = False),nn.ReLU(True))
    self.conv01 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[0], out_channels=args.conv_width[0], kernel_size=kernel_size, padding=padding,dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[0],track_running_stats = False),nn.ReLU(True))
    # self.drop_0 = nn.Dropout(p = coef_drop)
    self.conv10 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[0], out_channels=args.conv_width[1], kernel_size=kernel_size, padding=1, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[1],track_running_stats = False),nn.ReLU(True))
    self.conv11 = nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1], out_channels=args.conv_width[1], kernel_size=kernel_size, padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[1],track_running_stats = False), nn.ReLU(True))
    # self.drop_1 = nn.Dropout(p = coef_drop)
    #decoder
    self.conv20=nn.Sequential(nn.Conv2d(in_channels=args.conv_width[1] + args.conv_width[0], out_channels=args.conv_width[2], kernel_size=kernel_size, padding=1, padding_mode='reflect'),  nn.BatchNorm2d(args.conv_width[2],track_running_stats = False), nn.ReLU(True))
    self.conv21=nn.Sequential(nn.Conv2d(in_channels=args.conv_width[2], out_channels=args.conv_width[2], kernel_size=kernel_size,padding=padding, dilation = dilation, padding_mode='reflect'), nn.BatchNorm2d(args.conv_width[2],track_running_stats = False), nn.ReLU(True))
    
    self.convD=nn.Conv2d(in_channels=args.conv_width[2], out_channels=args.n_class, kernel_size=1, padding=0)
    
    self.cust = Custom_layer(args.Lambda_init)

    if args.cuda: #put the model on the GPU memory
      self.cuda()
    
  def forward(self, args, input, epoch):
    """
    the function called to run inference
    """ 
    if self.is_cuda: #put the data on the GPU
      input = input.cuda()
    #-----encoder----
    #level a
    x0 = self.conv01(self.conv00(input))
    x0down, indices_a_b = self.maxpool(x0)
    #level b
    x1=self.conv11(self.conv10(x0down))
    #-----decoder----
    #level a
    x1up = self.unpool(x1, indices_a_b)
    x2 = self.conv21(self.conv20(torch.cat((x1up[:,:int(x1up.shape[1]/2)],x0[:,:int(x0.shape[1]/2)],x1up[:,int(x1up.shape[1]/2):],x0[:,int(x0.shape[1]/2):]),1)))
    #régularisation
    if args.do_reg:
      #organisation des donnée par arête
      x3 = self.convD(x2)
      out = self.cust(args, x3,args.connectivities, epoch)
    else:
      out = self.convD(x2)
    
    return out
  
  def get_lambda(self):
    return self.cust.get_lambda()