import numpy as np
import torch
import torchnet as tnt
import torch.nn as nn
import torch.optim as optim
from tqdm import tqdm
import copy
import GPUtil


import modules.Confusion_Matrix as CM
import modules.network_definition as net_def

def get_repart():
  return net_def.get_repart()

def train_TV(model, optimizer, args, epoch, train_set):
  """train for one epoch"""
  model.train() #switch the model in training mode
  
  #the loader function will take care of the batching
  loader = torch.utils.data.DataLoader(train_set, \
         batch_size=args.batch_size, shuffle=True, drop_last=True)
  #tqdm will provide some nice progress bars
  loader = tqdm(loader)
    
  #will keep track of the loss
  loss_meter = tnt.meter.AverageValueMeter()
  cm = CM.ConfusionMatrix(args)

  for index, (tiles, gt) in enumerate(loader):

    if model.is_cuda: #put the ground truth on the GPU
      gt = gt.cuda()

    optimizer.zero_grad() #put gradient to zero

    pred = model(args, tiles, epoch) #compute the prediction

    loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99) #compute the loss

    loss.backward() #compute gradients

    print(model.cust.Lambda.grad)

    optimizer.step() #one SGD step

    loss_meter.add(loss.item())

    gt = gt.cpu() #back on the RAM

    labeled = np.where(gt.view(-1)!=99)[0] #select gt with a label, ie not 99
    #need to put the prediction back on the cpu and convert to numpy before feeding it to the CM
    cm.add_batch(gt.view(-1)[labeled], pred.argmax(1).view(-1)[labeled].cpu().detach().numpy())
    
  return cm, loss_meter.value()[0]

def eval_TV(model, args, epoch, img_set):
  """eval on test/validation set"""

  model.eval() #switch in eval mode
  
  loader = torch.utils.data.DataLoader(img_set, batch_size=1, shuffle=False, drop_last=False) 
  
  loader = tqdm(loader)
  
  loss_meter = tnt.meter.AverageValueMeter()
  cm = CM.ConfusionMatrix(args)

  with torch.no_grad(): #do not compute gradients (saves memory)
    for index, (tiles, gt) in enumerate(loader):
      
      if model.is_cuda: #put the ground truth on the GPU
        gt = gt.cuda()
      
      pred = model(args, tiles, epoch) #compute the prediction
      
      loss = nn.functional.cross_entropy(pred,gt,weight=args.weight_class, ignore_index=99) #compute the loss
      
      loss_meter.add(loss.item())

      gt = gt.cpu() #back on the RAM

      labeled = np.where(gt.view(-1)!=99)[0] #select gt with a label, ie not 99
      #need to put the prediction back on the cpu and convert to numpy before feeding it to the CM
      cm.add_batch(gt.view(-1)[labeled], pred.argmax(1).view(-1)[labeled].cpu().detach().numpy())
    
  return cm, loss_meter.value()[0]

def train_full_TV(args, train_set, validation_set, test_set):
  """The full training loop"""
  #initialize the model
  net_def.reset_repart()
  if hasattr(args,'retrain') and args.retrain:
    model = args.model
  else:
    model = net_def.SegNet(args) 

  #stock les valeurs pour les graph d'evolution
  evol_train=np.zeros((args.n_epoch,4))
  evol_valid=np.zeros((args.n_epoch,4))
  evol_test=np.zeros((args.n_epoch,4))
  evol_composante=np.zeros((args.n_epoch,4))
  compt_test=0

  print('Total number of parameters: {}'.format(sum([p.numel() for p in model.parameters()])))
  
  #define the optimizer
  #adam optimizer is always a good guess for classification
  if hasattr(args,'retrain') and args.retrain and args.blocage_reseau:
    optimizer = optim.Adam([
                            {'params': model.conv00.parameters()},
                            {'params': model.conv01.parameters()},
                            {'params': model.conv10.parameters()},
                            {'params': model.conv11.parameters()},
                            {'params': model.conv20.parameters()},
                            {'params': model.conv21.parameters()},
                            {'params': model.convD.parameters()},
                            {'params': model.cust.parameters(), 'lr': args.lr}], lr=0)
  else:
    optimizer = optim.Adam(model.parameters(), lr=args.lr)

  scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.2)
  
  TESTCOLOR = '\033[104m'
  TRAINCOLOR = '\033[100m'
  VALIDCOLOR = '\033[101m'
  NORMALCOLOR = '\033[0m'

  i_epoch = 0
  compt_verif = 0
  max_IoU = 0
  # min_comp = 65536
  save_model = copy.deepcopy(model)
  epoch_save = 0

  if hasattr(args,'retrain') and args.retrain:
    cm_test, loss_test= eval_TV(model, args, 12, test_set)
    print(TESTCOLOR)
    print('Test %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f' % \
        (compt_test, cm_test.overall_accuracy()*100, cm_test.class_IoU(), loss_test) + NORMALCOLOR)

  while i_epoch < args.n_epoch and compt_verif < args.n_epoch_validation:
    #train one epoch
    cm_train, loss_train = train_TV(model, optimizer, args, i_epoch, train_set)

    evol_composante[i_epoch,0]=i_epoch
    evol_composante[i_epoch,1]=torch.mean(torch.tensor(args.compte_composante).float())
    args.compte_composante = []
    
    print(TRAINCOLOR)
    print('Epoch %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f' % \
          (i_epoch, cm_train.overall_accuracy()*100, cm_train.class_IoU(), loss_train) + NORMALCOLOR)
    evol_train[i_epoch,0]=i_epoch
    evol_train[i_epoch,1]=cm_train.overall_accuracy()
    evol_train[i_epoch,2]=cm_train.class_IoU()
    evol_train[i_epoch,3]=loss_train

    cm_valid, loss_valid= eval_TV(model, args, i_epoch, validation_set)

    evol_composante[i_epoch,2]=torch.mean(torch.tensor(args.compte_composante).float())
    args.compte_composante = []

    print(VALIDCOLOR)
    print('Validation %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f' % \
        (i_epoch, cm_valid.overall_accuracy()*100, cm_valid.class_IoU(), loss_valid) + NORMALCOLOR)
    evol_valid[i_epoch,0]=i_epoch
    evol_valid[i_epoch,1]=cm_valid.overall_accuracy()
    evol_valid[i_epoch,2]=cm_valid.class_IoU()
    evol_valid[i_epoch,3]=loss_valid

    new_model = False
    # sauvegarde dans le cas d'un nouveau model mieux que le precedants
    if not args.do_reg or i_epoch >= args.blocage_epoch + (max(args.blocage_reg, args.red_grad_lambda)*2):
      if max_IoU <= evol_valid[i_epoch,2]: # and evol_composante[i_epoch,2] <= min_comp:
        compt_verif = 0
        new_model = True
        max_IoU = evol_valid[i_epoch,2]
        # min_comp = evol_composante[i_epoch,2]
        save_model = copy.deepcopy(model)
        epoch_save = i_epoch
      else:
        compt_verif = compt_verif +1
    
    scheduler.step()

    if (i_epoch == args.n_epoch - 1) or new_model or compt_verif >= args.n_epoch_validation:
      #periodic testing
      net_def.reset_repart()
      cm_test, loss_test= eval_TV(save_model, args, epoch_save, test_set)

      evol_composante[i_epoch,3]=torch.mean(torch.tensor(args.compte_composante).float())
      args.compte_composante = []
      
      print(TESTCOLOR)
      print('Test %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f' % \
          (compt_test, cm_test.overall_accuracy()*100, cm_test.class_IoU(), loss_test) + NORMALCOLOR)
      evol_test[compt_test,0]=compt_test
      evol_test[compt_test,1]=cm_test.overall_accuracy()
      evol_test[compt_test,2]=cm_test.class_IoU()
      evol_test[compt_test,3]=loss_test
      compt_test=compt_test+1

    i_epoch = i_epoch + 1 
    print("\n")
  
  return [save_model,evol_train,evol_test,evol_valid,evol_composante,epoch_save]