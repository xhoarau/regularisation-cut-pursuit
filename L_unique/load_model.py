import modules.storage_class as sc
import modules.save_result as sr
import modules.insertion_donne as insert
import modules.train_fonctions as tf
import modules.Confusion_Matrix as CM
import modules.network_definition as net_def
import modules.plot_evol as pl_ev
import modules.plot_exemple_result as pl_re
import os

import numpy as np
import torch

name = "Lambda_unique_retrain_batchnorm_update"
print(name)

evol = False # plot l'evolution au cours de l'apprentissage
evaluation = False # evalure le modèle sur l'ensemble d'entrainement

img = True # generer une image avec des exemples de resultats
train = False # choix de l'ensemble
n_shown = 3 #nombre d'image a traité
category = "cgpC" 
  # if 'c' in category : rgb color
  # if 'i' in category : infrared
  # if 'g' in category : ground truth
  # if 'p' in category : predictiosn
  # if 'e' in category : error
  # if 'C' in category : Composante
L_pred = False # choix d'affichage des Lambda gt ou pred


path = "Models/"
if os.path.exists(path):
  resultat = sr.load(path + "resultat_" + name + ".pt")
  args = sr.load(path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    resultat = sr.load(path + "resultat_" + name + ".pt")
    args = sr.load(path + "args_" + name + ".pt")

print("durée entrainement(s) = ",resultat.duree_entrainement)

print("lambda value = ",resultat.trained_model.get_lambda())

if evaluation:
  test_set = insert.creation_set(args)[0]
  cm, loss = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set)
  print('Test %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f' % \
          (resultat.epoch_save, cm.overall_accuracy()*100, cm.class_IoU(), loss))
  List_composante = net_def.get_composante()
  print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante).float()))
  print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante).float()))


if evol:
  pl_ev.plot_evol_IoU(resultat.evol_train_TV, "Train_" + name, resultat.epoch_save)
  pl_ev.plot_evol_IoU(resultat.evol_valid_TV, "Valid_" + name, resultat.epoch_save)
  pl_ev.plot_evol_IoU(resultat.evol_test_TV, "test_" + name, resultat.epoch_save)
  if args.do_reg:
    pl_ev.plot_evol_coposante(resultat.evol_composante, args, name, resultat.epoch_save)

if img:
  pl_re.viewer(args, resultat.trained_model, resultat.epoch_save, name = name, n_shown = n_shown, category = category, train = train, L_pred=L_pred)

Lambdas = [2.223]
test_set = insert.creation_set(args)[0]
args.do_reg = True
result = np.zeros([18,4])
i=0
for L in Lambdas:
  state_dict = resultat.trained_model.cust.state_dict()
  state_dict['Lambda'] = torch.tensor(L)
  resultat.trained_model.cust.load_state_dict(state_dict)
  print(resultat.trained_model.get_lambda())
  cm, loss = tf.eval_TV(resultat.trained_model, args, resultat.epoch_save, test_set)
  print('Test %3d -> Overall Accuracy: %3.2f%% mIoU : %3.2f%% Loss: %1.4f' % \
          (L, cm.overall_accuracy()*100, cm.class_IoU(), loss))
  result[i,0] = cm.class_IoU()
  result[i,1] = loss
  List_composante = net_def.get_composante()
  print("nb composante moyenne : ", torch.mean(torch.tensor(List_composante[len(List_composante)-100:]).float()))
  print("ecart type nb de composante : ", torch.std(torch.tensor(List_composante[len(List_composante)-100:]).float()))
  result[i,2] = torch.mean(torch.tensor(List_composante[len(List_composante)-100:]).float())
  result[i,3] = torch.std(torch.tensor(List_composante[len(List_composante)-100:]).float())
  i=i+1

sr.save(result,"resultat_train_0")

name = "Lambda_unique_retain_init1_blocage"
print(name)


path = "Models/"
if os.path.exists(path):
  resultat_2 = sr.load(path + "resultat_" + name + ".pt")
  args_2 = sr.load(path + "args_" + name + ".pt")
else:
  path = os.path.join(os.getcwd(),os.pardir,"Models/")
  if os.path.exists(path):
    resultat_2 = sr.load(path + "resultat_" + name + ".pt")
    args_2 = sr.load(path + "args_" + name + ".pt")
  else:
    path = os.getcwd()
    resultat_2 = sr.load(path + "resultat_" + name + ".pt")
    args_2 = sr.load(path + "args_" + name + ".pt")

# print(resultat.trained_model.conv00.state_dict())
# print(resultat_2.trained_model.conv00.state_dict())
